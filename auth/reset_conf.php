<?php 
$page_title = "Reset Confirmation";
include_once("blocks/header.php");
include_once("blocks/reset_conf_block.php");
?>
<body>
<div class="container">
<section class="col col-lg-7">
    <h2>Password Reset</h2>
    <hr>

    <?php 
    if (isset($result)) echo $result;
    if (!empty($form_err)) echo show_errors($form_err);
    ?>
    <form method="post" action="">
    <table>
    
    <tr>
    <td>
    Email:
    </td>
    <td>
    <input type="email" value="" name="email">
    </td>
    </tr>

    <tr>
    <td>
    New Password:
    </td>
    <td>
    <input type="text" value="" name="new_password">
    </td>
    </tr>

    <tr>
    <td>
    Confrim Password:
    </td>
    <td>
    <input type="text" value="" name="confirm_password">
    </td>
    </tr>

    <tr>
    <td>
    Submit
    </td>
    <td>
    <input type="submit" name="reset_btn" value="Reset Passsword">
    </td>
    </tr>

    </table>
    </form>
    <p>
    <script>
    document.write('<a href="' + document.referrer + '">Back</a>');
    </script>
    &nbsp; <a href="index.php">Home</a> &nbsp; <a href="signup.php">Sign Up</a>
    </p>
</section>
</div>
</body>
<?php
if ($pw_reset == 1) {
    echo "
    <script type='text/javascript'>
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Password reset successful!',
        text: 'You can now log in...',
        showConfirmButton: true
    }).then(function() {
        window.location.href = 'index.php';
    });
    </script>";
} else if ($pw_reset == 2) {
    echo "<script type='text/javascript'>
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Reset error!',
        text: 'Please try again...',
        confirmButtonText: 'Okay',
    });
    </script>";
}
?>

<?php
include_once("blocks/footer.php");
?>



