<?php
/* 
Edited: 08/22/2021 
By: David Ayodele

References:
Terry Osayawe: https://www.youtube.com/playlist?list=PL9mwDhhLGZilsMTm1POos9aXjvorie3Me
https://thedigitalcraft.com
*/
define('scsn', TRUE); // NEEDS TO BE BEFORE DB INCLUDE
include_once('../../db_conn/auth_db.php');    //rel ref must be from final call loc (signup.php, login.php, EXCEPT when using ajax!)

/**
 * @param $fields, an array of form field names
 * @return array, an array containing form field names
 */
function form_empty_check($required_fields) {
    // initializes array to store any error msgs
    $form_err = array();

    //checks if each required field is set, if not, stores in error array
    foreach($required_fields as $field){
        if(!isset($_POST[$field]) || $_POST[$field] == NULL){
            $form_err[] = "no ".$field;
        }
    }
    return $form_err;
}


/**
 * @param $required_fields, an array of form field names whose minimum required length is unknown (e.g. username => 100)
 * @return array, an array containing form field names in violation
 */
function field_len_check($required_fields) {
    // initializes array to store any error msgs
    $form_err = array();

    //checks if each required field meets min lenght requirements, if not, stores in error array
    foreach($required_fields as $field => $min_len_reqd){
        if(strlen(trim($_POST[$field])) <  $min_len_reqd) {
            $form_err[] = $field . " is too short, must be at least {$min_len_reqd} characters long";
        }
    }
    return $form_err;
}


/**
 * @param $field, an array with a key/value pair where key is the field name, value is the value entered. 
 * @return array, an array containing email field violations
 */
function email_check($field) {
    // initializes array to store any error msgs
    $form_err = array();
    $key = 'email';

    //checks if the field name is "email"
    if(array_key_exists($key, $field)) {
        //checks if value is NULL
        if($_POST[$key] != NULL) {
            //removes illegal chars from email
            $key = filter_var($key, FILTER_SANITIZE_EMAIL);

            //checks if email is in valid format
            if(filter_var($_POST[$key], FILTER_VALIDATE_EMAIL) === false) {
                $form_err[] = $key . " is an invalid email address";
            }
        }
    }
    return $form_err;
}


/**
 * @param $passwords, an array of form field passwords that must match
 * @return string, a message indicating the match status 
 */
function password_match($passwords) {
    // initializes array to store any error msgs
    $form_err = array();

    //casts passwords into assoc. array and checks if they match
    foreach($passwords as $password1 => $password2){
        if($password1 != $password2) {
            $form_err[] = "Passwords ({$password1} and {$password2}) DO NOT match. Try again.";
        }
    }
    return $form_err;
}

/**
 * @param $cred, an array containing the key and value of the user's credential (e.g. 'email' => value)
 * @return string, a message indicating the user's existance in the database.
 */
function find_user($cred) {
    //casts passwords into assoc. array and checks if they match
    foreach($cred as $key => $val){
        try {
            $query = "SELECT * FROM users WHERE email = :email";
            $statement = $db->prepare($query);
            $statement->execute(array(':email' => $val));

            // if fetch succeeds, store result in $row
            if ($row = $statement->fetch()) {
                $id = $row['id'];
                $sec_pass = $row['password'];
                $email = $row['email'];

                if (password_verify($pass, $sec_pass)) {
                    $_SESSION['id'] = $id;
                    $_SESSION['email'] = $email;
                    //header("location: index.php");
                    page_hop("index");
                } else {
                    //$result = "<p style='color: red;'>Invalid email or password</p>";
                    login_msg("Error: Invalid email or password");
                }
            }
        } catch (PDOException $ex) {
            //$result = "<p style='color: red;'>Error: Invalid email or password</p>";
            log_msg("Error: Invalid email or password");
        }
    }
    return $result;
}

/**
 * @param $seconds, an integer denoting seconds
 * @return a string of formatted seconds in hours & minutes
 */
function time_disp($seconds){
    $h = floor(($seconds/60)/60); // Hours
    $m = round(($seconds/60)) - ($h * 60); // Minutes
    echo '<span class="hours">'.$h.'</span> hrs : <span class="minutes">'.$m.'</span> mins'; // Display result   
}

function log_msg($msg = "", $color = "red") {
    if ($color === "green") {
        $str = "<p style='padding: 15px; color: green; border: 1px solid gray;'>{$msg}</p>";
    } else {
        $str = "<p style='padding: 15px; color: red; border: 1px solid gray;'>{$msg}</p>";
    }
    return $str;
}

function page_hop($page) {
    header("Location: {$page}.php");
}

function dup_email($email, $db) {
    try {
        $query = "SELECT username FROM users WHERE username = :username";
        $statement = $db->prepare($query);
        $statement->execute(array(':username' => $email));

        $row = $statement->fetch();
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $ex) {
        // handle exception
        echo log_msg("Error: ".$ex.getMessage(), "red");
    }
}

function dup_phone($phone, $db) {
    try {
        $query = "SELECT * FROM users WHERE phone = :phone";
        $statement = $db->prepare($query);
        $statement->execute(array(':phone' => $phone));

        $row = $statement->fetch();
        if (!empty($row)) {
            return true;
        } else {
            return false;
        }
    } catch (PDOException $ex) {
        // handle exception
        echo log_msg("Error: ".$ex.getMessage(), "red");
    }
}

/**
 * @param $errors, an array of form errors 
 * @return string, a list of error messages
 */
function show_errors($errors) {
    $error_list = "<p><ul style='padding: 10px 30px 10px; color: red; border: 1px solid gray;'>";
    
    //displays list items as HTML
    foreach($errors as $error) {
        $error_list .= "<li> {$error} </li>"; 
    }

    $error_list .= "</ul></p>";
    return $error_list;
}

if (!function_exists('encryptString')) {
    function encryptString($plaintext, $password, $encoding = null) {
        $iv = openssl_random_pseudo_bytes(16);
        $ciphertext = openssl_encrypt($plaintext, "AES-256-CBC", hash('sha256', $password, true), OPENSSL_RAW_DATA, $iv);
        $hmac = hash_hmac('sha256', $ciphertext.$iv, hash('sha256', $password, true), true);
        return $encoding == "hex" ? bin2hex($iv.$hmac.$ciphertext) : ($encoding == "base64" ? base64_encode($iv.$hmac.$ciphertext) : $iv.$hmac.$ciphertext);
    }
}

if (!function_exists('decryptString')) {
    function decryptString($ciphertext, $password, $encoding = null) {
        $ciphertext = $encoding == "hex" ? hex2bin($ciphertext) : ($encoding == "base64" ? base64_decode($ciphertext) : $ciphertext);
        $known_str = hash_hmac('sha256', substr($ciphertext, 48).substr($ciphertext, 0, 16), hash('sha256', $password, true), true);
        $usr_str = substr($ciphertext, 16, 32);
        settype ($usr_str, "string");
        if (!hash_equals($known_str, $usr_str)) return null;
        return openssl_decrypt(substr($ciphertext, 48), "AES-256-CBC", hash('sha256', $password, true), OPENSSL_RAW_DATA, substr($ciphertext, 0, 16));
    }
}
/**
 * @param $email, a string 
 */
function remember_me($email) {
    $key = "CEDOYmeUaQteh5i4y3dnt";
    $encrypted_cookie = encryptString($email, $key);
    setcookie('rem_cookie', $encrypted_cookie, 
    [
        'expires' => time()+60*60*24*7, // 7 days
        'path' => '/',
        //'domain' => 'domain.com',
        'secure' => true,
        //'httponly' => true,
        'samesite' => 'Lax',
    ]);
}

/**
 * checks if cookie found matches cookie for user
 * @param $db, database connection link
 * @return bool, true if user cookie is valid
 */
function valid_cookie($db) {
    $valid = false;
    $key = "CEDOYmeUaQteh5i4y3dnt";

    if (isset($_COOKIE['rem_cookie'])) {
        $cipher = $_COOKIE['rem_cookie'];
        //echo "ENCRYPTED COOKIE IS: ".$_COOKIE['rem_cookie'];
        $decrypted_cookie = decryptString($cipher, $key);
        //echo "COOKIE IS: ".$decrypted_cookie;
        
        $email = $decrypted_cookie;

        $query = "SELECT * FROM users WHERE email = :email";
        $statement = $db->prepare($query);
        $statement->execute(array(':email' => $email));

        $row = $statement->fetch();
        if (!empty($row)) {
            $email = $row['email'];
            $fname = $row['fname'];

            $_SESSION['email'] = $email;
            $_SESSION['fname'] = $fname;
            $valid = true;
        } else {
            $valid = false;
            //echo "COOKIE IS: ".$decrypted_cookie;
            signout();
        }
    }
    return $valid;
}

if (!function_exists('valid_img')) {
    function valid_img($file) {
        $form_err = array();

        //splits file name into parts using delim (makes lowercase first)
        /*
        $parts = explode(".", strtolower($file['name']));
        $file_type = end($parts);
        $img_types = [".jpg", ".jpeg", ".png", ".gif", ".tiff", ".bmp", ".webp"];
        */

        $formats = ["image/jpeg", "image/bmp", "image/gif", "image/heic", "image/png", "image/tiff", "image/svg+xml"];

        $upload_err_list = array( 
            1 => 'Max upload size exceeded - see php.ini',
            2 => 'Max upload size exceeded - see form',
            3 => 'Incomplete upload',
            4 => 'No upload',
            // #5 is deprecated
            6 => 'Missing temporary folder',
            7 => 'Disk write failure',
            8 => 'Upload stopped by an extension',
        );

        if (!in_array($file['type'], $formats)) {
            $form_err[] = $file['type'] . " is not an allowed format.";
            return $form_err;
        } else {
            return $form_err;
        }

        if ($file['error'] != 0) {
            $form_err[] = "Error: ". $upload_err_list[$file['error']];
            return $form_err;
        } else {
            return $form_err;
        }
    }
}

if (!function_exists('signout')) {
    function signout(){
        //var_dump($_SESSION);
        foreach ($_SESSION as $var) {
            unset($_SESSION[$var]);
        }

        if(isset($_COOKIE['rem_cookie'])){
            unset($_COOKIE['rem_cookie']);
            setcookie('rem_cookie', null, -1, '/');
        }
        //var_dump($_SESSION);

        echo 
        "
        <script src='//cdn.jsdelivr.net/npm/sweetalert2@11'>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Goodbye!',
            text: 'You\'re being logged out...',
            showConfirmButton: false,
            timer: 3000
        });

        setTimeout(function(){
            /*window.location.href = 'index.php';*/
        }, 3000);
        </script>
        ";
        session_destroy();
        //session_regenerate_id(true);
        page_hop("logout");
        //var_dump($_SESSION);
    }
}

if (!function_exists('save')) {
    function save($data, $path){
        //echo("encoding data as json...<br>");
        $json_data = json_encode($data); // Convert data array back to json
        //echo("successfully encoded data.<br>");
        //echo("writing to file...<br>");
        $file = fopen($path, "w") or die("Unable to open file!"); // Open file
        fwrite($file, $json_data); // Save file with jsaon data
        //echo("succesfully written, closing file: $path <br>");
        fclose($file);
        //echo("file closed.<br>");
    }
}

if (!function_exists('get_ip_address')) {
    function get_ip_address(){
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key){
            if (array_key_exists($key, $_SERVER) === true){
                foreach (explode(',', $_SERVER[$key]) as $ip){
                    $ip = trim($ip); // just to be safe

                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false){
                        return $ip;
                    }
                }
            }
        }
    }
}

function get_user_ip() {
    // Get real visitor IP behind CloudFlare network
    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
              $_SERVER['REMOTE_ADDR'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
              $_SERVER['HTTP_CLIENT_IP'] = $_SERVER["HTTP_CF_CONNECTING_IP"];
    }
    $client  = @$_SERVER['HTTP_CLIENT_IP'];
    $forward = @$_SERVER['HTTP_X_FORWARDED_FOR'];
    $remote  = $_SERVER['REMOTE_ADDR'];

    if(filter_var($client, FILTER_VALIDATE_IP)){
        $ip = $client;
    }
    elseif(filter_var($forward, FILTER_VALIDATE_IP)){
        $ip = $forward;
    }
    else{
        $ip = $remote;
    }
    return $ip;
}

if (!function_exists('get_ip_address')) {
     /**
     * converts 2D array to 1D rowwise
     * @param $array, a 2D array
     * @return array, the final 1D array 
     */
    function array_flatten($array) { 
        if (!is_array($array)) { 
            return FALSE; 
        } 
        $result = array(); 
        foreach ($array as $key => $value) { 
            if (is_array($value)) { 
            $result = array_merge($result, array_flatten($value)); 
            } 
            else { 
            $result[$key] = $value; 
            } 
        } 
        return $result; 
    }
}
?>
