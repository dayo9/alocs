<?php

/* 
sessions create a file, server-side, which stores session data (key-val pairs & data types). 
A (functional) cookie is then stored, client-side, that contains the hex key of the session file.
The header of subsequent requests to the server from that client contain the key. 
This allows the server to "link" with the client  
In PHP, the hex value is passed to APACHE/NGINX/etc which then pass it to PHP script, which then appends the prefix "sess_" to it
and looks for a file on the server matching that name. If found, it then checks the key-value pairs and in the file and presents the matching value.  
*/ 
session_start();

?>
