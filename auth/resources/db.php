<!-- 
Edited: 12/12/2020 
By: David Ayodele

References:
Terry Osayawe: https://www.youtube.com/playlist?list=PL9mwDhhLGZilsMTm1POos9aXjvorie3Me
https://thedigitalcraft.com
-->

<?php
include_once('utils.php');

// Connect to MySQL 
error_reporting(E_ERROR | E_PARSE);
define('scsn', TRUE);

session_start(); //storing session data
date_default_timezone_set('America/Phoenix');

$time = date('H:i:s');
$date = date('Y-m-d');
$datetime = "$date $time";
echo "Date is ".$date.", Time is ".$time."<br>";

$db_host = "localhost";
$db_user = "root";
$db_name = "scsn_db";
$db_pass = "";

try {
    $db_link = new PDO("mysql:host=$db_host;", $db_user, $db_pass);
    $db_link->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Successfully connected to database<br>";
} catch (PDOException $ex) {
    echo "Database connection error: ".$ex->getMessage()."<br>";
}

$db_table1 = "users";
$db_table2 = "enrollments";
$db_table3 = "cpr_enrollments";
$db_table4 = "aid_enrollments";

$table1_col1_type = "usr_id int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY not null";
$table1_col2_type = "fname varchar(255) not null DEFAULT 'NA'";
$table1_col3_type = "lname varchar(255) not null DEFAULT 'NA'";
$table1_col4_type = "mname varchar(255) not null DEFAULT 'NA'";
$table1_col5_type = "addr varchar(255) not null DEFAULT 'NA'";
$table1_col6_type = "city varchar(255) not null DEFAULT 'NA'";
$table1_col7_type = "zip varchar(255) not null DEFAULT 'NA'";
$table1_col8_type = "phone varchar(255) not null DEFAULT 'NA'"; 
$table1_col9_type = "email varchar(100) UNIQUE not null";
$table1_col10_type = "dobm varchar(255) not null DEFAULT 'NA'";
$table1_col11_type = "dobd varchar(2) not null DEFAULT 'NA'"; // cannot use 'N/A' since it has 3 chars
$table1_col12_type = "reg_date timestamp not null"; // datetime type cannot have 'N/A' as default value
$table1_col13_type = "usr_name varchar(30) UNIQUE not null"; 
$table1_col14_type = "usr_pwd varchar(255) not null DEFAULT 'NA'";
$table1_col15_type = "salt mediumtext not null"; // mediumtext cannot have a default value

$table1_col1 = "usr_id";
$table1_col2 = "fname";
$table1_col3 = "lname";
$table1_col4 = "mname";
$table1_col5 = "addr";
$table1_col6 = "city";
$table1_col7 = "zip";
$table1_col8 = "phone"; 
$table1_col9 = "email";
$table1_col10 = "dobm";
$table1_col11 = "dobd";
$table1_col12 = "reg_date"; 
$table1_col13 = "usr_name"; 
$table1_col14 = "usr_pwd";
$table1_col15 = "salt";

// Make selected db the current database
echo "selecting db in server...<br>";
$init_query = "CREATE DATABASE $db_name";
$query0 = "USE $db_name";
$query1 = "create TABLE $db_table1 (
    $table1_col1_type, 
    $table1_col2_type,
    $table1_col3_type,
    $table1_col4_type,
    $table1_col5_type,
    $table1_col6_type,
    $table1_col7_type,
    $table1_col8_type,
    $table1_col9_type,
    $table1_col10_type,
    $table1_col11_type,
    $table1_col12_type,
    $table1_col13_type,
    $table1_col14_type,
    $table1_col15_type
);";
//$query2 = "INSERT INTO $db_table1 ($table1_col1, $table1_col2, $table1_col3) VALUES (0, 'test', 'TEST')";
        
try {
    $init_query_result = $db_link->query($init_query);
    //$stat = $db_link->prepare($init_query);
    //$stat->execute([':db_name' => $db_name]);
    echo "New database successfully created!<br>";

    try {
        $query0_result = $db_link->query($query0);
        //$stat = $db_link->prepare($query0);
        //$stat->execute([':db_name' => $db_name]);
        echo("QUERY 0 SUCCESSFUL<br>");
    } catch (PDOException $ex) {
        echo "QUERY 0 Error: ".$ex->getMessage()."<br>";
    }
    
    try {
        $query1_result = $db_link->query($query1);
        //$stat = $db_link->prepare($query1);
        //$stat->execute([':db_table1' => $db_table1, ':table1_col1_type' => $table1_col1_type, ':table1_col2_type' => $table1_col2_type, ':table1_col3_type' => $table1_col3_type, ':table1_col4_type' => $table1_col4_type, ':table1_col5_type' => $table1_col5_type, ':table1_col6_type' => $table1_col6_type, ':table1_col7_type' => $table1_col7_type, ':table1_col8_type' => $table1_col8_type, ':table1_col9_type' => $table1_col9_type, ':table1_col10_type' => $table1_col10_type, ':table1_col11_type' => $table1_col11_type, ':table1_col12_type' => $table1_col12_type, ':table1_col13_type' => $table1_col13_type, ':table1_col14_type' => $table1_col14_type]);
        echo("QUERY 1 SUCCESSFUL<br>");
    } catch (PDOException $ex) {
        echo "Query 1 error: ".$ex->getMessage()."<br>";
    }
    
    /*
    try {
        $query2_result = $db_link->query($query2);
        echo("QUERY 2 SUCCESSFUL<br>");
    } catch (PDOException $ex) {
        echo "Query 2 error: ".$ex->getMessage()."<br>";
    }*/

    var_dump($_POST);
    $data = $_POST; // Store data array, form serialization necessary for json auto increment
    print_r($data);
    
    if (isset($_POST['entry_953935801'])) { // 
        $cf1_fname = $_POST['entry_953935801'];
        $cf1_lname = $_POST['entry_2130693918'];
        $cf1_mname = $_POST['entry_1109337040'];
        $cf1_addr = $_POST['entry_577968961'];
        $cf1_city = $_POST['entry_527484810'];
        $cf1_zip = $_POST['entry_1836094135'];
        $cf1_phone = $_POST['entry_1189187901'];
        $cf1_email = $_POST['entry_647512161'];
        $cf1_dobm = $_POST['entry_1774485035'];
        $cf1_dobd = $_POST['entry_700377545'];
        $cf1_usrname = $_POST['entry_37808800'];
        $cf1_pwd = $_SESSION['pwd1'];
        $cf1_salt = $_SESSION["salt"];
        
        echo("email: $cf1_email<br>");
        echo("pwd: $cf1_pwd<br>");
        //print_r($_POST);
    
        // Insert values in table 1
        try {
            //$query2_result = $db_link->query($query2);
            $query2 = "INSERT INTO $db_table1 ($table1_col1, $table1_col2, $table1_col3, $table1_col4, $table1_col5, $table1_col6, $table1_col7, $table1_col8, $table1_col9, $table1_col10, $table1_col11, $table1_col12, $table1_col13, $table1_col14, $table1_col15) VALUES (0, :fname, :lname, :mname, :addr, :city, :zip, :phone, :email, :dobm, :dobd, now(), :usr_name, :usr_pwd, :salt)";
            $stat = $db_link->prepare($query2);
            $stat->execute(array(':fname' => $cf1_fname, ':lname' => $cf1_lname, ':mname' => $cf1_mname, ':addr' => $cf1_addr, ':city' => $cf1_city, ':zip' => $cf1_zip, ':phone' => $cf1_phone, ':email' => $cf1_email, ':dobm' => $cf1_dobm, ':dobd' => $cf1_dobd, ':usr_name' => $cf1_usrname, ':usr_pwd' => $cf1_pwd, ':salt' => $cf1_salt));
            echo("QUERY 2 SUCCESSFUL<br>");
        } catch (PDOException $ex) {
            echo "Query 2 error: ".$ex->getMessage()."<br>";
        }

        //---------------------------
        // --- Store in JSON file ---
        //---------------------------
        echo("encoding form data for JSON...<br>");
    
        $contents = file_get_contents($db_table1."_data.json"); // Will create if needed -- file_get_contents("../../db_conn/".$db_table1."_data.json");
        $tempArray = json_decode($contents, 1); // 1 for ASSOC = TRUE
        //array_push($tempArray, $data);

        try {
            $query3 = $db_link->prepare("SELECT `$table1_col1` FROM `$db_table1` ORDER BY `$table1_col1` DESC LIMIT 1");
            $query3->execute([$table1_col1]);
            echo("QUERY 3 SUCCESSFUL<br>");
            $count = $query3->fetchColumn();
            echo("USER ID = <br>".$count);
        } catch (PDOException $ex) {
            echo "QUERY 3 Error: ".$ex->getMessage()."<br>";
        }        

        $time = time();
        $tempArray[$time]['cf1_usrid'] = $count;
        $tempArray[$time]['cf1_fname'] = $cf1_fname; // PHP requires "." in $_POST to be "_"
        $tempArray[$time]['cf1_lname'] = $cf1_lname;
        $tempArray[$time]['cf1_mname'] = $cf1_mname;
        $tempArray[$time]['cf1_addr'] = $cf1_addr;
        $tempArray[$time]['cf1_city'] = $cf1_city;
        $tempArray[$time]['cf1_zip'] = $cf1_zip;
        $tempArray[$time]['cf1_phone'] = $cf1_phone;
        $tempArray[$time]['cf1_email'] = $cf1_email;
        $tempArray[$time]['cf1_dobm'] = $cf1_dobm;
        $tempArray[$time]['cf1_dobd'] = $cf1_dobd;
        $tempArray[$time]['cf1_usrname'] = $cf1_usrname;
        $tempArray[$time]['cf1_pwd'] = $cf1_pwd;
        $tempArray[$time]['cf1_salt'] = $cf1_salt;
        save($tempArray, $db_table1."_data.json");        
    } 

} catch (PDOException $ex) {
    echo "INIT QUERY Error: ".$ex->getMessage()."<br>";
    var_dump($_POST);
    $data = $_POST; // Store data array, form serialization necessary for json auto increment
    print_r($data);
    
    if (isset($_POST['entry_953935801'])) { // 
        $cf1_fname = $_POST['entry_953935801'];
        $cf1_lname = $_POST['entry_2130693918'];
        $cf1_mname = $_POST['entry_1109337040'];
        $cf1_addr = $_POST['entry_577968961'];
        $cf1_city = $_POST['entry_527484810'];
        $cf1_zip = $_POST['entry_1836094135'];
        $cf1_phone = $_POST['entry_1189187901'];
        $cf1_email = $_POST['entry_647512161'];
        $cf1_dobm = $_POST['entry_1774485035'];
        $cf1_dobd = $_POST['entry_700377545'];
        $cf1_usrname = $_POST['entry_37808800'];
        $cf1_pwd = $_SESSION['pwd1'];
        $cf1_salt = $_SESSION["salt"];
        
        echo("email: $cf1_email<br>");
        echo("pwd: $cf1_pwd<br>");
        //print_r($_POST);

        // Select DB
        try {
            $query0_result = $db_link->query($query0);
            //$stat = $db_link->prepare($query0);
            //$stat->execute([':db_name' => $db_name]);
            echo("QUERY 0 SUCCESSFUL<br>");
        } catch (PDOException $ex) {
            echo "QUERY 0 Error: ".$ex->getMessage()."<br>";
        }
        
        // Insert values in table 1
        try {
            //$query2_result = $db_link->query($query2);
            $query2 = "INSERT INTO $db_table1 ($table1_col1, $table1_col2, $table1_col3, $table1_col4, $table1_col5, $table1_col6, $table1_col7, $table1_col8, $table1_col9, $table1_col10, $table1_col11, $table1_col12, $table1_col13, $table1_col14, $table1_col15) VALUES (0, :fname, :lname, :mname, :addr, :city, :zip, :phone, :email, :dobm, :dobd, now(), :usr_name, :usr_pwd, :salt)";
            $stat = $db_link->prepare($query2);
            $stat->execute(array(':fname' => $cf1_fname, ':lname' => $cf1_lname, ':mname' => $cf1_mname, ':addr' => $cf1_addr, ':city' => $cf1_city, ':zip' => $cf1_zip, ':phone' => $cf1_phone, ':email' => $cf1_email, ':dobm' => $cf1_dobm, ':dobd' => $cf1_dobd, ':usr_name' => $cf1_usrname, ':usr_pwd' => $cf1_pwd, ':salt' => $cf1_salt));
            echo("QUERY 2 SUCCESSFUL<br>");
        } catch (PDOException $ex) {
            echo "Query 2 error: ".$ex->getMessage()."<br>";
        }

        //---------------------------
        // --- Store in JSON file ---
        //---------------------------
        echo("encoding form data for JSON...<br>");
    
        $contents = file_get_contents($db_table1."_data.json"); // Will create if needed -- file_get_contents("../../db_conn/".$db_table1."_data.json");
        $tempArray = json_decode($contents, 1); // 1 for ASSOC = TRUE
        //array_push($tempArray, $data);

        try {
            $query3 = $db_link->prepare("SELECT `$table1_col1` FROM `$db_table1` ORDER BY `$table1_col1` DESC LIMIT 1");
            $query3->execute([$table1_col1]);
            echo("QUERY 3 SUCCESSFUL<br>");
            $count = $query3->fetchColumn();
            echo("USER ID = <br>".$count);
        } catch (PDOException $ex) {
            echo "QUERY 3 Error: ".$ex->getMessage()."<br>";
        }        

        $time = time();
        $tempArray[$time]['cf1_usrid'] = $count;
        $tempArray[$time]['cf1_fname'] = $cf1_fname; // PHP requires "." in $_POST to be "_"
        $tempArray[$time]['cf1_lname'] = $cf1_lname;
        $tempArray[$time]['cf1_mname'] = $cf1_mname;
        $tempArray[$time]['cf1_addr'] = $cf1_addr;
        $tempArray[$time]['cf1_city'] = $cf1_city;
        $tempArray[$time]['cf1_zip'] = $cf1_zip;
        $tempArray[$time]['cf1_phone'] = $cf1_phone;
        $tempArray[$time]['cf1_email'] = $cf1_email;
        $tempArray[$time]['cf1_dobm'] = $cf1_dobm;
        $tempArray[$time]['cf1_dobd'] = $cf1_dobd;
        $tempArray[$time]['cf1_usrname'] = $cf1_usrname;
        $tempArray[$time]['cf1_pwd'] = $cf1_pwd;
        $tempArray[$time]['cf1_salt'] = $cf1_salt;
        save($tempArray, $db_table1."_data.json");        
    } 

}

//PDO link is automatically closed when script ends

?>  
<!-- 
Notes

https://stackoverflow.com/questions/9068767/php-mysql-create-database-if-not-exists
https://stackoverflow.com/questions/8705195/how-do-i-select-a-mysql-database-to-use-with-pdo-in-php
https://stackoverflow.com/questions/11928136/return-one-value-from-database-with-mysql-php-pdo/11928442
-->