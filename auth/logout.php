<?php 
include_once('resources/sessions.php');
include_once('resources/utils.php');

if (isset($_SESSION['fname'])) {
    $id = decryptString($_SESSION['id'], "user_id");
    $login_time = $_SESSION['login_time'];
    $user_ip = $_SESSION['user_ip'];
    $prior_url = $_SESSION['prior_url'];
    $url = decryptString($_SESSION['users_sheet'], "users_sheet");
    $fname = $_SESSION['fname'];

    try {
        /*==========
        GSheet Log
        ==========*/
        $fields = array(
            'user_id' => $id,
            'login_time' => $login_time, // datetime from database.php
            'user_ip' => $user_ip,
            'prior_url' => $prior_url,
            'logout_time' => $datetime,
        );
    
        $curl_obj = curl_init($url);
        curl_setopt($curl_obj, CURLOPT_URL, $url);
        @curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
        @curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
        @curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
        $headers = array(
            "Content-Type: application/json",
            "Access-Control-Allow-Origin: *",
        );
        @curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        @curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
    
        $GLOBALS['status_code_http'] = @curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
        $GLOBALS['status_code_https'] = @curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
        $curl_reply = @curl_exec($curl_obj); // executes curl connection and stores reply values in var
    
        @curl_close($curl_obj); // terminates curl 
    
        /*==========
        JSON Log
        ==========*/
        $file_path = $user_logs_json;  // file is be created manually to avoid permission issues
        if (file_exists($file_path)) {
            $js_data = file_get_contents($file_path);
        } else {
            file_put_contents($file_path, ''); //creates file if non-existent
            $js_data = file_get_contents($file_path);
        }
        $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
    
        if (is_array($js_array)) { // error handling if no data
            krsort($js_array); // reverse key sort lib function
            $js_length = sizeof($js_array);
        } else {
            //echo("<script>console.log('js_array is empty');</script>");
        }
    
        $js_array[$datetime]['user_id'] = $id;
        $js_array[$datetime]['login_time'] = $login_time; // datetime from database.php
        $js_array[$datetime]['user_ip'] = $user_ip;
        $js_array[$datetime]['prior_url'] = $prior_url;
        $js_array[$datetime]['logout_time'] = $datetime;
    
        save($js_array, $file_path); // save func in utils
    
        /*==========
        SQL Log
        ==========*/
        $query = "INSERT INTO user_logs (usr_id, login_time, user_ip, prior_url, logout_time) 
        VALUES (:usr_id, :login_time, :user_ip, :prior_url, :logout_time)";
                        
        try {
            $statement = $db->prepare($query);
            $statement->execute(array(':usr_id' => $id, ':login_time' => $login_time, ':user_ip' => $user_ip, ':prior_url' => $prior_url, ':logout_time' => $datetime));
        } catch (PDOException $ex) {
            $result = log_msg("Error: ".$ex->getMessage(), "red");
        }
        
        signout();
    } catch (Exception $ex) {
        $result = log_msg("Error: ".$ex->getMessage(), "red");
    } 
    
    //session_destroy();
    //header('location: index.php');
} 
?>


<?php 
$page_title = "Logout";
include_once("blocks/header.php");
?>
<body>
<div class="container">
<section class="col col-lg-7">
<h2>You are logged out.</h2>
<hr>

<?php
include_once("blocks/footer.php");
?>






