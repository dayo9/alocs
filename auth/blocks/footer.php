<div class="container">
<footer class="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
    <div class="d-flex align-items-center">
    <a href="/" class="mb-3 me-2 mb-md-0 text-muted text-decoration-none lh-1">
    <svg class="bi" width="30" height="24"><use xlink:href="#bootstrap"></use></svg>
    </a>
    <span class="text-muted"><span class="copyleft_logo">©</span>2021 <a href="http://alocs.com">ALOCS</a></span>
    </div>
    <br>
    <!--
    <script>
    document.write('<a href="' + document.referrer + '">Back</a>');
    </script>
    &nbsp; <a href="index.php">Home</a>
    -->
    <ul class="nav soc_media col-md-6 col-md-offset-3 justify-content-end list-unstyled d-flex">
    <!-- <li class="ms-3"><a class="text-muted" href="#"><svg class="bi" width="24" height="24"><use xlink:href="#gmail"></use></svg></a></li> -->
    <li class="ms-3"><a class="text-muted" href="https://www.instagram.com/anotherlevelcommunity/"><svg class="bi" width="24" height="24"><use xlink:href="#instagram"></use></svg></a></li>
    <li class="ms-3"><a class="text-muted" href="https://www.facebook.com/anotherlevelcommunity/"><svg class="bi" width="24" height="24"><use xlink:href="#facebook"></use></svg></a></li>
    </ul>
</footer>
</div>
<script src="js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
<script src="js/custom.js"></script>

<script>
$(".toggle-password").click(function() {
    $(".toggle-password").toggleClass("fa-eye fa-lock"); /*$(this).toggleClass("fa-eye-slash") */
    //var input = $($(this).attr("toggle"));
    var input = $(".toggle")

    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});
</script>

<!-- Apple iframe cropping issues script -->
<script>
 $(function(){
if (/iPhone|iPod|iPad/.test(navigator.userAgent))
    $('iframe').wrap(function(){
        var $this = $(this);
        return $('<div />').css({
            width: $this.attr('width'),
            height: $this.attr('height'),
            overflow: 'auto',
            '-webkit-overflow-scrolling': 'touch'
        });
    });
})
</script>
<!-- End Apple iframe cropping issues script -->

<!-- Recaptcha call script -->
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<!-- End Recaptcha call script -->
<style>

.soc_media {
    margin-left: auto;
    margin-right: auto;
}

body {
    font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

.cf_container {
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    /* padding-left: 10px; */
    /* padding-right: 10px; */
}

input#catcher_id, input#uname, input#salt {
    visibility: hidden;
}

.cf_frame {
    height: 100%;
    width: 80%;
    box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.33);
    background: linear-gradient( rgba(80,45,87,0.75), rgba(80,45,87,0.95)), no-repeat center center;
    background-size: cover;
    margin-left: auto;
    margin-right: auto;
    border-top: solid 1px rgba(255,255,255,.5);
    border-radius: 5px;
    box-shadow: 1px 3px 7px 1px rgba(0,0,0,0.33);
    overflow: hidden;
    transition: all .5s ease;
}

.cf_form {
    /* width: 100%; */
    height: 100%;
    font-size: 16px;
    top: 0px;
    font-weight: 300;
    padding-left: 25px;
    padding-right: 45px;
    padding-top: 10px;
    transition: opacity .5s ease, transform .5s ease;
}

.cf_form a {
    color: white;
}

.cf_form-styling {
    font-family: FontAwesome, 'Open Sans', Arial, Helvetica;
    font-style: normal;
    font-weight: normal;
    color: #fff;
    text-decoration: inherit;
    width: 100%;
    height: 35px;
    padding-left: 35px;
    border: none;
    border-radius: 20px;
    margin-bottom: 20px;
    background: rgba(255,255,255,.2);
}

.cf_form-styling option {
    background: rgb(133,108,137);
}

.cf_heading {
    color: #fff;
    font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

.cf_form input:focus, textarea:focus {
    background: rgba(255,255,255,.25);
    border: none !important;
    outline: none !important;
    transition: .2s ease;
}

.copyleft_logo {
  display: inline-block;
  transform: rotate(180deg) !important;
}

.error_list { 
    color: white; 
    font-weight: bold;
}

.red_msg {
    height: 15px;
    width: auto;
    font-size: 0.8rem;
    color: rgb(250, 0, 0) !important;
    margin-bottom: 50px;
    transition: all ease 0.25s;
}

span#status_msg {
    color: rgb(255, 150, 50);
    margin-top: 48px !important;
}

.green_msg {
    height: 15px;
    width: auto;
    font-size: 0.8rem;
    color: rgb(103, 250, 28) !important;
    margin-bottom: 50px;
    transition: all ease 0.25s;
}

h2.cf_heading {
    margin-bottom: 0px;
    margin-top: 10px;
    font-weight: bold;
    /* color: rgba(250,149,28, 1); */
    text-shadow: 0.5px 0.5px 0.5px rgb(0 0 0 / 33%);
}

h6.cf_heading {
    margin-top: 0px;
    margin-bottom: 10px;
}

i.fa {
    position: absolute;
    margin-left: 15px;
    margin-top: 38px;
    color: rgba(255, 255, 255, 0.5);
}

i.fa.fa-lock, i.fa.fa-eye {
    position: absolute;
    margin-top: 10px;
    margin-left: 15px;
    color: rgba(255, 255, 255, 0.5);
    cursor: pointer;
}

/*https://www.iconfinder.com/icons/226587/clock_icon*/
input[type="date"]::-webkit-calendar-picker-indicator {
    /*color: rgba(255, 255, 255, 1);*/ 
    opacity: 0.6;
    display: block;
    background: url('../img/icons/calendar_icon2_15x16.png') no-repeat; /*https://mywildalberta.ca/images/GFX-MWA-Parks-Reservations.png*/
    width: 15px;
    height: 15px;
    padding-right: 10px;
    border-width: thin;
}

input::placeholder, textarea::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: rgba(255, 255, 255, 0.5);
    opacity: 1; /* Firefox */
}

input:-ms-input-placeholder, textarea:-ms-inut-placeholder { /* Internet Explorer 10-11 */
    color: rgba(255, 255, 255, 0.5);
}

input::-ms-input-placeholder, textarea::-ms-inut-placeholder { /* Microsoft Edge */
    color: rgba(255, 255, 255, 0.5);
}

select {
    color: #fff !important;
}

#ss-submit:disabled {
    opacity: 0;
    cursor: default;
}

#ss-submit:enabled {
    opacity: 1;
    cursor: pointer;
}

label {
    font-weight: 400;
    text-transform: uppercase;
    font-size: 13px;
    padding-left: 15px;
    padding-bottom: 10px;
    color: rgba(255,255,255,.7);
    display: block;
}

textarea#msg {
    padding-top: 10px;
    color: #ffffff;
    border-radius: 25px;
    width: 94%;
    height: 50px !important;
}

.btn-submit {
    float: left;
    padding-top: 0px;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 20px;
    margin-top: -8px;
}

.btn-animate {
    float: left;
    font-weight: 700;
    text-transform: uppercase;
    font-size: 13px;
    text-align: center;
    color: rgba(255,255,255, 1);
    padding-top: 0px;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 20px;
    margin-top: 15px;
    margin-bottom: 40px;
    background-color: #438fff;
    left: 0px;
    top: 0px;
    transition: all .5s ease, top .5s ease .5s, height .5s ease .5s, background-color .5s ease .75s;
}

.btn-submit:hover {
    cursor: pointer;
    background-color: #3473d0;
    transition: background-color .5s;
}

/*
@media screen and (min-width: 772px) {
  #captchabox {
    transform: scale(0.6);
    margin-left: -37%;
  }
}

@media screen and (min-width: 889px) {
  #captchabox {
    transform: scale(0.7);
    margin-left: -20%;
  }
}
*/
</style>


</html>
<?php 
/*
<!--Notes

https://stackoverflow.com/questions/9068767/php-mysql-create-database-if-not-exists
https://stackoverflow.com/questions/8705195/how-do-i-select-a-mysql-database-to-use-with-pdo-in-php
https://stackoverflow.com/questions/11928136/return-one-value-from-database-with-mysql-php-pdo/11928442 

-->
*/
?>