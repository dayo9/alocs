<?php
include_once('resources/utils.php');
$pw_reset = 0;

if (isset($_GET['id'])) {
    $sec_user_id = $_GET['id'];
    $id = explode("i_am_the_greenest_geek", base64_decode($sec_user_id))[1];
    //$id = decryptString($enc_id, "user_verification"); 
}

if (isset($_POST['reset_btn'])) {
    // makes array to hold form errors
    $form_err = array();

    //validates login form
    $reqd_fields = array('email', 'new_password', 'confirm_password');
    $form_err = array_merge($form_err, form_empty_check($reqd_fields));
    
    // performs min length check
    $reqd_lenghts = array('new_password' => 5, 'confirm_password' => 5); 
    $form_err = array_merge($form_err, field_len_check($reqd_lenghts));

    // performs email validation
    $form_err = array_merge($form_err, email_check($_POST));

    // performs passwords match check
    $passwords = array($_POST['new_password'] => $_POST['confirm_password']);
    $form_err = array_merge($form_err, password_match($passwords));


    if (empty($form_err) && isset($_POST['reset_btn'])) {
        //check for user in db
        $email = $_POST['email'];
        $new_pass = $_POST['new_password'];

        try {
            /*==========
            SQL Storage
            ==========*/
            $query = "SELECT * FROM users 
            JOIN user_photos
            ON users.id = usr_id
            WHERE id = :id";

            $statement = $db->prepare($query);
            $statement->execute(array(':id' => $id));

            // if fetch succeeds, store result in $row, insert new password
            if ($row = $statement->fetch()) {
                $id = $row['id'];
                $old_pass = $row['password'];
                $username = $row['username'];
                $fname = $row['fname'];
                $lname = $row['lname'];
                $dob = $row['dob'];
                $phone = $row['phone'];
                $street = $_POST['street'];
                $city = $_POST['city'];
                $zip = $_POST['zip'];
                
                $sec_passwd = password_hash($new_pass, PASSWORD_BCRYPT);

                $img_path = $row['photo_loc'];
                $verified = $row['verified'];
                $reset_link = "Hi $fname, your password was reset at $datetime. Please contact us if this is an error.";

                // updates password
                $query = "UPDATE users SET password = :password WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':password' => $sec_passwd, ':id' => $id));

                /*==========
                GSheets Storage
                ==========*/

                $sheets_url = 'https://script.google.com/macros/s/AKfycbxb3DH4VNL585thGxuLhKqLSchW6lL1xm4smKxd8SQ_7sYBOyeUi1lMUWjLJjNq_LZxyQ/exec';

                $fields = array(
                    'datetime' => $datetime,
                    'user_id' => $id,
                    'user_ip' => "",  // used for script control
                    'fname' => $fname,
                    'lname' => $lname,
                    'dob' => $dob,
                    'phone' => $phone,
                    'email' => $email,
                    'street' => $street,
                    'city' => $city,
                    'zip' => $zip,
                    'password' => $sec_passwd,
                    'user_photo' => $img_path,
                    "verified" => $verified,
                    "reset_link" => $reset_link,
                    "edited" => "password",
                );
                
                $curl_obj = curl_init($sheets_url);
                curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                $headers = array(
                    "Content-Type: application/json",
                    "Access-Control-Allow-Origin: *",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                
                $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                //for debug only!
                //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                
                $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                
                curl_close($curl_obj); // terminates curl 
            
                $array = json_decode($curl_reply); // converts curl response to array from JSON
                
                /*==========
                End GSheets Storage
                ==========*/

                /*==========
                JSON Storage
                Thanks: thedigitalcraft.com
                ==========*/
                $file_path = "$users_json";  // file is be created manually to avoid permission issues
                if (file_exists($file_path)) {
                    $js_data = file_get_contents($file_path);
                } else {
                    file_put_contents($file_path, ''); //creates file if non-existent
                    $js_data = file_get_contents($file_path);
                }
                $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                
                if (is_array($js_array)) { // error handling if no data
                    krsort($js_array); // reverse key sort lib function
                    $js_length = sizeof($js_array);
                } else {
                    //echo("<script>console.log('js_array is empty');</script>");
                }
                
                $js_array[$datetime]['time'] = $datetime;
                $js_array[$datetime]['user_id'] = $usr_id;
                $js_array[$datetime]['fname'] = $fname;
                $js_array[$datetime]['lname'] = $lname;
                $js_array[$datetime]['dob'] = $dob;
                $js_array[$datetime]['phone'] = $phone;
                $js_array[$datetime]['email'] = $email;
                $js_array[$datetime]['street'] = $street;
                $js_array[$datetime]['city'] = $city;
                $js_array[$datetime]['zip'] = $zip;
                $js_array[$datetime]['password'] = $sec_passwd;
                $js_array[$datetime]['user_photo'] = $img_path;
                $js_array[$datetime]['verified'] = $verified;
                $js_array[$datetime]['reset_link'] = $reset_link;
                $js_array[$datetime]['edited'] = "password";

                save($js_array, $file_path); // save func in utils
                /*==========
                End JSON Storage
                ==========*/

                $result = log_msg("Password reset successful!", "green");
                $pw_reset = 1;
                // trigger alert
            } else {
                $result = log_msg("Email or phone number not found. Please try again (remember to add the phone country code).");
                $pw_reset = 2;
            }
        } catch (PDOException $ex) {
            $result = log_msg("Error: ".$ex->getMessage());
        }
    } else {
        if (count($form_err) >= 1) {
            $result = log_msg("Error count: ".count($form_err));
        }
    }
}

?>