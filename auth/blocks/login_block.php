<?php
// include_once('resources/sessions.php'); (sessions already included in header. will be included after header)
include_once('resources/sessions.php');
include_once('resources/utils.php'); // will be included on login page (in main auth dir)

if (isset($_POST['login_btn'])) {

    // makes array to hold form errors
    $form_err = array();

    //validates login form
    $reqd_fields = array('email', 'password');
    $form_err = array_merge($form_err, form_empty_check($reqd_fields));

    if (empty($form_err)) {
        //check for user in db
        $email = $_POST['email'];
        $pass = $_POST['password'];

        // look for remember me cookie and store in $rem_user if exists
        isset($_POST['rem_check']) ? $rem_user = $_POST['rem_check'] : $rem_user = "";

        try {
            // gets all photos of user, then returns most recent
            $query = "SELECT * FROM users 
            RIGHT JOIN user_photos ON users.id = user_photos.usr_id
            WHERE email = :email
            ORDER BY photo_id DESC
            LIMIT 1";

            $statement = $db->prepare($query);
            $statement->execute(array(':email' => $email));
            $row = $statement->fetch(); // WILL BE NULL/EMPTY IF NOT FOUND
            if (empty($row)) $result = log_msg("Invalid email or password", "red");

            // if fetch succeeds, store result in $row
            if (!empty($row)) {  //USE IF NOT WHILE (IGNORE TERRY)
                $id = $row['id'];
                $photo_id = $row['photo_id'];
                $photo_loc = $row['photo_loc'];
                $sec_pass = $row['password'];
                $email = $row['email'];
                $fname = $row['fname'];
                $lname = $row['lname'];
                $phone = $row['phone'];
                $dob = $row['dob'];
                $street = $row['street'];
                $city = $row['city'];
                $zip = $row['zip'];
                $join_date = $row['join_date'];
                $verified = $row['verified'];
                $reset_link = $row['reset_link'];
                $user_ip = get_user_ip();
                $prior_url = $_SERVER['HTTP_REFERER'];

                if ($verified == "0") {
                    $result = log_msg("Unverified account! Please check your phone or email.", "red");
                } else {
                    if (password_verify($pass, $sec_pass)) {
                        //session_start();
                        $_SESSION['id'] = encryptString($id, "user_id");
                        $_SESSION['email'] = $email;
                        $_SESSION['fname'] = $fname;
                        $_SESSION['lname'] = $lname;
                        $_SESSION['phone'] = $phone;
                        $_SESSION['dob'] = $dob;
                        $_SESSION['street'] = $street;
                        $_SESSION['city'] = $city;
                        $_SESSION['zip'] = $zip;
                        $_SESSION['join_date'] = $join_date;
                        $_SESSION['photo_loc'] = $photo_loc;
    
                        $_SESSION['password'] = $sec_pass;
                        $_SESSION['verified'] = $verified;
                        $_SESSION['reset_link'] = $reset_link;
                        $_SESSION['login_time'] = $datetime;
                        $_SESSION['photo_id'] = $photo_id;
                        $url = 'https://script.google.com/macros/s/AKfycbxb3DH4VNL585thGxuLhKqLSchW6lL1xm4smKxd8SQ_7sYBOyeUi1lMUWjLJjNq_LZxyQ/exec';
                        $_SESSION['users_sheet'] = encryptString($url, "users_sheet");
    
                        $_SESSION['user_ip'] = $user_ip;
                        $_SESSION['prior_url'] = $prior_url;
    
                        /*==========
                        GSheet Log
                        ==========*/
                        $fields = array(
                            'user_id' => $id,
                            'login_time' => $datetime, // datetime from database.php
                            'user_ip' => $user_ip,
                            'prior_url' => $prior_url,
                            'logout_time' => null,
                        );
    
                        $curl_obj = curl_init($url);
                        curl_setopt($curl_obj, CURLOPT_URL, $url);
                        curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                        curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                        $headers = array(
                            "Content-Type: application/json",
                            "Access-Control-Allow-Origin: *",
                        );
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                        
                        $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                        $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                        $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                        
                        curl_close($curl_obj); // terminates curl 
    
                        /*==========
                        JSON Log
                        ==========*/
                        $file_path = $user_logs_json;  // file is be created manually to avoid permission issues
                        if (file_exists($file_path)) {
                            $js_data = file_get_contents($file_path);
                        } else {
                            file_put_contents($file_path, ''); //creates file if non-existent
                            $js_data = file_get_contents($file_path);
                        }
                        $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                        
                        if (is_array($js_array)) { // error handling if no data
                            krsort($js_array); // reverse key sort lib function
                            $js_length = sizeof($js_array);
                        } else {
                            //echo("<script>console.log('js_array is empty');</script>");
                        }
                        
                        $js_array[$datetime]['user_id'] = $id;
                        $js_array[$datetime]['login_time'] = $datetime; // datetime from database.php
                        $js_array[$datetime]['user_ip'] = $user_ip;
                        $js_array[$datetime]['prior_url'] = $prior_url;
                        $js_array[$datetime]['logout_time'] = null;
    
                        save($js_array, $file_path); // save func in utils
    
                        /*==========
                        SQL Log
                        ==========*/
                        $sql_ins = "INSERT INTO user_logs (usr_id, login_time, user_ip, prior_url, logout_time) 
                        VALUES (:usr_id, :login_time, :user_ip, :prior_url, :logout_time)";
                        try {
                            $statement = $db->prepare($sql_ins);
                            $statement->execute(array(':usr_id' => $id, ':login_time' => $datetime, ':user_ip' => $user_ip, ':prior_url' => $prior_url, ':logout_time' => '0000-00-00'));
                        } catch (PDOException $ex) {
                            $result = log_msg("Error: ".$ex->getMessage(), "red");
                        }                
    
                        // if remember me checkbox value is "yes" set cookie w/ 7 day timer
                        if($rem_user === "yes") {
                            remember_me($email);
                        }
    
                        // trigger alert
                        /*
                        echo "<script type='text/javascript'>
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Welcome $fname!',
                            text: 'You\'re being logged in...',
                            showConfirmButton: false,
                            timer: 2000
                          });
    
                          setTimeout(function(){
                              window.location.href = 'index.php';
                          }, 2000);
                          </script>";
                          */
                        
                        //page_hop("index");  //(compiler will parse PHP before JS so alert won't show before hop!)
                    } else {
                        $result = log_msg("Invalid email or password", "red");
                    }   
                }
            }
        } catch (PDOException $ex) {
            $result = log_msg("Error: ". $ex->getMessage(), "red");
        }
    } else {
        if (count($form_err) >= 1) {
            $result = log_msg("Error count: ".count($form_err), "red");
        }
    }
}

?>