<?php 
include_once("resources/database.php");
include_once("resources/utils.php");

//var_dump($_SESSION);
$sheets_url = decryptString($_SESSION['users_sheet'], "users_sheet");
$fname = $_SESSION['fname'];
$form_err = array();

if ( (isset($_SESSION['phone']) || isset($_GET['user_id'])) && !isset($_POST['edit_btn']) ) {
    $email = $_SESSION['email'];
    $phone = $_SESSION['phone'];
    $id = decryptString($_SESSION['id'], "user_id");
    $sheets_url = decryptString($_SESSION['users_sheet'], "users_sheet");
    
    if (isset($_GET['id'])) {
        $url_encoded_id = $_GET['id'];
        $url_decoded_id = base64_decode($url_encoded_id);
        $url_id_arr = explode("happygoid", $url_decoded_id);
        $id = $url_id_arr[1];
        //echo $id;
    }

    $def_img = "resources/uploads/profile/def_profile_pic2.png";

    if (!empty($_SESSION['photo_loc'])) {
        $acct_img = $_SESSION['photo_loc'];
    } else {
        $acct_img = $def_img;
    }

    $sec_id = base64_encode("happygoid{$id}");
    
} else if (isset($_POST['edit_btn'])) {
    /*=========================
    ALWAYS CHANGE IF EXPANDING
    ==========================*/
    
    // performs field checks for all cases
    $result = "";
    //echo "<script type='text/javascript'> var alerts = []; </script>";

    if ( !empty($_POST['fname']) && !empty($_POST['lname']) && !empty($_POST['phone']) && !empty($_POST['dob']) && !empty($_POST['street']) && !empty($_POST['city']) && !empty($_POST['zip']) ) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $fname = $_POST['fname'];
        $lname = $_POST['lname'];
        $phone = $_POST['country_code']."-".$_POST['phone'];
        $dob = $_POST['dob'];
        $street = $_POST['street'];
        $city = $_POST['city'];
        $zip = $_POST['zip'];

        $form_err = array();
        $reqd_lenghts = array('fname' => 1);
        $form_err = array_merge($form_err, field_len_check($reqd_lenghts));

        // performs email validation
        $form_err = array_merge($form_err, email_check($_POST));

        // perform duplicate email check
        if (dup_phone($phone, $db)) {
            $result = log_msg("Phone number already exists.");
            // trigger alert
            echo "<script type='text/javascript'>
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'OOPS!',
                text: '$phone already exists, please login or try again.',
                showConfirmButton: true,
            });
            </script>";    
        }
    
        if (empty($form_err)) {
            // processes form if no errors found
        
            try {
                /*=========
                SQL Storage
                ==========*/
                echo "<script type='text/javascript'>
                    swal({
                        title: 'Updating database...'
                    });
                    swal.showLoading();
                    </script>";

                $query = "UPDATE users SET fname = :fname, lname = :lname, phone = :phone, dob = :dob, street = :street, city = :city, zip = :zip WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':fname' => $fname, ':lname' => $lname, ':phone' => $phone, ':dob' => $dob, ':street' => $street, ':city' => $city, ':zip' => $zip, ':id' => $id));

                if($statement->rowCount() == 1) {
                    $result = log_msg("Last name, first name, and phone edits successful!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                    /*==========
                    GSheets Storage
                    ==========/
                    
                    $fields = array(
                        'datetime' => $datetime,
                        'user_id' => $id,
                        'fname' => $fname,
                        'lname' => $lname,
                        'dob' => $_SESSION['dob'],
                        'phone' => $phone,
                        'street' => $street,
                        'city' => $city,
                        'zip' => $zip,
                        'email' => $_SESSION['email'],
                        'password' => $_SESSION['password'],
                        'user_photo' => $_SESSION['photo_loc'],
                        "verified" => $_SESSION['verified'],
                        "reset_link" => $reset_link,
                        "edited" => "fname, lname, phone, street, city, zip",
                    );
                    
                    $curl_obj = curl_init($sheets_url);
                    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                    $headers = array(
                        "Content-Type: application/json",
                        "Access-Control-Allow-Origin: *",
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                    
                    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                    //for debug only!
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    
                    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                    
                    curl_close($curl_obj); // terminates curl 
                    
                    //var_dump($curl_reply);
                    //echo "";
                    //print_r($curl_reply);
                    $array = json_decode($curl_reply); // converts curl response to array from JSON
                    
                    /*==========
                    End GSheets Storage
                    ==========*/

                    /*==========
                    JSON Storage
                    Thanks: thedigitalcraft.com
                    ==========*/
                    $file_path = $users_json;  // file is be created manually to avoid permission issues
                    if (file_exists($file_path)) {
                        $js_data = file_get_contents($file_path);
                    } else {
                        file_put_contents($file_path, ''); //creates file if non-existent
                        $js_data = file_get_contents($file_path);
                    }
                    $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                    
                    if (is_array($js_array)) { // error handling if no data
                        krsort($js_array); // reverse key sort lib function
                        $js_length = sizeof($js_array);
                    } else {
                        //echo("<script>console.log('js_array is empty');</script>");
                    }
                    
                    $js_array[$datetime]['time'] = $datetime;
                    $js_array[$datetime]['user_id'] = $id;
                    $js_array[$datetime]['fname'] = $fname;
                    $js_array[$datetime]['lname'] = $lname;
                    $js_array[$datetime]['dob'] = $dob;
                    $js_array[$datetime]['phone'] = $phone;
                    $js_array[$datetime]['street'] = $street;
                    $js_array[$datetime]['city'] = $city;
                    $js_array[$datetime]['zip'] = $zip;
                    $js_array[$datetime]['email'] = $_SESSION['email'];
                    $js_array[$datetime]['password'] = $_SESSION['password'];
                    $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                    $js_array[$datetime]['verified'] = $_SESSION['verified'];
                    $js_array[$datetime]['reset_link'] = $reset_link;
                    $js_array[$datetime]['edited'] = "fname, lname, phone, street, city, zip";

                    save($js_array, $file_path); // save func in utils
                    /*==========
                    End JSON Storage
                    ==========*/

                    // trigger alert
                    echo "<script type='text/javascript'>
                    swal.close();
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Edit(s) saved! Logout to view changes.',
                        showConfirmButton: true
                    }).then(function() {
                        window.location.href = 'account.php';
                    });
                    </script>";
                } else {
                    $result = log_msg("Error, duplicate or invalid entry", "red");
                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'OOPS!',
                        text: 'No changes made!',
                        showConfirmButton: true
                    });
                    </script>";
                }
            } catch (PDOException $ex) {
                //$result = log_msg("Error: ".$ex->getMessage());
    
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'An error occured, please try again.',
                    showConfirmButton: true
                });
                </script>";
            }    
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                //var_dump($form_err);
                // $result = "<p style='color: red;'> Error:</p>";
                // $result .= "<ul style='color: red;'>";
                
                // foreach($form_err as $err) {
                //    $result .= "<li> no {$err} </li>";
                // }
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    }
    /*==========
     First Name
    ============*/
    if (!empty($_POST['fname'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $fname = $_POST['fname']; 
        $form_err = array();
        $reqd_lenghts = array('fname' => 1);
        $form_err = array_merge($form_err, field_len_check($reqd_lenghts));

        if (empty($form_err)) {
            // processes form if no errors found    
            try {    
                $query = "UPDATE users SET fname = :fname WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':fname' => $fname, ':id' => $id));

                if($statement->rowCount() == 1) {
                    $result .= log_msg("First name edit successful!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                    
                    /*==========
                    GSheets Storage
                    ==========/
                    $fields = array(
                        'datetime' => $datetime,
                        'user_id' => $id,
                        'fname' => $fname,
                        'lname' => $_SESSION['fname'],
                        'dob' => $_SESSION['dob'],
                        'phone' => $_SESSION['phone'],
                        'street' => $_SESSION['street'],
                        'city' => $_SESSION['city'],
                        'zip' => $_SESSION['zip'],
                        'email' => $_SESSION['email'],
                        'password' => $_SESSION['password'],
                        'user_photo' => $_SESSION['photo_loc'],
                        "verified" => $_SESSION['verified'],
                        "reset_link" => $reset_link,
                        "edited" => "fname",
                    );
                    
                    $curl_obj = curl_init($sheets_url);
                    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                    $headers = array(
                        "Content-Type: application/json",
                        "Access-Control-Allow-Origin: *",
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                    
                    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                    //for debug only!
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    
                    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                    
                    curl_close($curl_obj); // terminates curl 
                    
                    //var_dump($curl_reply);
                    //echo "";
                    //print_r($curl_reply);
                    $array = json_decode($curl_reply); // converts curl response to array from JSON
                    
                    /*==========
                    End GSheets Storage
                    ==========*/

                    /*==========
                    JSON Storage
                    Thanks: thedigitalcraft.com
                    ==========*/
                    $file_path = $users_json;  // file is be created manually to avoid permission issues
                    if (file_exists($file_path)) {
                        $js_data = file_get_contents($file_path);
                    } else {
                        file_put_contents($file_path, ''); //creates file if non-existent
                        $js_data = file_get_contents($file_path);
                    }
                    $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                    
                    if (is_array($js_array)) { // error handling if no data
                        krsort($js_array); // reverse key sort lib function
                        $js_length = sizeof($js_array);
                    } else {
                        //echo("<script>console.log('js_array is empty');</script>");
                    }
                    
                    $js_array[$datetime]['time'] = $datetime;
                    $js_array[$datetime]['user_id'] = $id;
                    $js_array[$datetime]['fname'] = $fname;
                    $js_array[$datetime]['lname'] = $_SESSION['lname'];
                    $js_array[$datetime]['dob'] = $_SESSION['dob'];
                    $js_array[$datetime]['phone'] = $_SESSION['phone'];
                    $js_array[$datetime]['street'] = $_SESSION['street'];
                    $js_array[$datetime]['city'] = $_SESSION['city'];
                    $js_array[$datetime]['zip'] = $_SESSION['zip'];
                    $js_array[$datetime]['email'] = $_SESSION['email'];
                    $js_array[$datetime]['password'] = $_SESSION['password'];
                    $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                    $js_array[$datetime]['verified'] = $_SESSION['verified'];
                    $js_array[$datetime]['reset_link'] = $reset_link;
                    $js_array[$datetime]['edited'] = "fname";

                    save($js_array, $file_path); // save func in utils
                    /*==========
                    End JSON Storage
                    ==========*/

                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Edit(s) saved! Logout to view changes.',
                        showConfirmButton: true
                    }).then(function() {
                        window.location.href = 'account.php';
                    });
                    </script>";
                } else {
                    $result = log_msg("Error, duplicate or invalid entry", "red");
                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'OOPS!',
                        text: 'No changes made!',
                        showConfirmButton: true
                    });
                    </script>";
                }
            } catch (PDOException $ex) {
                //$result = log_msg("Error: ".$ex->getMessage());
    
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'An error occured, please try again.',
                    showConfirmButton: true
                });
                </script>";
            }    
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                //var_dump($form_err);
                // $result = "<p style='color: red;'> Error:</p>";
                // $result .= "<ul style='color: red;'>";
                
                // foreach($form_err as $err) {
                //    $result .= "<li> no {$err} </li>";
                // }
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    }
    /*==========
     Last Name
    ============*/
    if (!empty($_POST['lname'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $lname = $_POST['lname']; 
        try {    
            $query = "UPDATE users SET lname = :lname WHERE id = :id";
            $statement = $db->prepare($query);
            $statement->execute(array(':lname' => $lname, ':id' => $id));

            if($statement->rowCount() == 1) {
                $result .= log_msg("Last name edit successful!", "green");
                $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                
                /*==========
                GSheets Storage
                ==========/
                $fields = array(
                    'datetime' => $datetime,
                    'user_id' => $id,
                    'fname' => $_SESSION['fname'],
                    'lname' => $lname,
                    'dob' => $_SESSION['dob'],
                    'phone' => $_SESSION['phone'],
                    'street' => $_SESSION['street'],
                    'city' => $_SESSION['city'],
                    'zip' => $_SESSION['zip'],
                    'email' => $_SESSION['email'],
                    'password' => $_SESSION['password'],
                    'user_photo' => $_SESSION['photo_loc'],
                    "verified" => $_SESSION['verified'],
                    "reset_link" => $reset_link,
                    "edited" => "lname",
                );
                
                $curl_obj = curl_init($sheets_url);
                curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                $headers = array(
                    "Content-Type: application/json",
                    "Access-Control-Allow-Origin: *",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                
                $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                //for debug only!
                //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                
                $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                
                curl_close($curl_obj); // terminates curl 
                
                //var_dump($curl_reply);
                //echo "";
                //print_r($curl_reply);
                $array = json_decode($curl_reply); // converts curl response to array from JSON
                
                /*==========
                End GSheets Storage
                ==========*/

                /*==========
                JSON Storage
                Thanks: thedigitalcraft.com
                ==========*/
                $file_path = $users_json;  // file is be created manually to avoid permission issues
                if (file_exists($file_path)) {
                    $js_data = file_get_contents($file_path);
                } else {
                    file_put_contents($file_path, ''); //creates file if non-existent
                    $js_data = file_get_contents($file_path);
                }
                $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                
                if (is_array($js_array)) { // error handling if no data
                    krsort($js_array); // reverse key sort lib function
                    $js_length = sizeof($js_array);
                } else {
                    //echo("<script>console.log('js_array is empty');</script>");
                }
                
                $js_array[$datetime]['time'] = $datetime;
                $js_array[$datetime]['user_id'] = $id;
                $js_array[$datetime]['fname'] = $_SESSION['fname'];
                $js_array[$datetime]['lname'] = $lname;
                $js_array[$datetime]['dob'] = $_SESSION['dob'];
                $js_array[$datetime]['phone'] = $_SESSION['phone'];
                $js_array[$datetime]['street'] = $_SESSION['street'];
                $js_array[$datetime]['city'] = $_SESSION['city'];
                $js_array[$datetime]['zip'] = $_SESSION['zip'];
                $js_array[$datetime]['email'] = $_SESSION['email'];
                $js_array[$datetime]['password'] = $_SESSION['password'];
                $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                $js_array[$datetime]['verified'] = $_SESSION['verified'];
                $js_array[$datetime]['reset_link'] = $reset_link;
                $js_array[$datetime]['edited'] = "lname";

                save($js_array, $file_path); // save func in utils
                /*==========
                End JSON Storage
                ==========*/

                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Edit(s) saved! Logout to view changes.',
                    showConfirmButton: true
                }).then(function() {
                    window.location.href = 'account.php';
                });
                </script>";
            } else {
                $result = log_msg("Error, duplicate or invalid entry", "red");
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'No changes made!',
                    showConfirmButton: true
                });
                </script>";
            }
        } catch (PDOException $ex) {
            //$result = log_msg("Error: ".$ex->getMessage());

            // trigger alert
            echo "<script type='text/javascript'>
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'OOPS!',
                text: 'An error occured, please try again.',
                showConfirmButton: true
            });
            </script>";
        }  
    }
    /*==========
     Date of Birth
    ============*/
    if (!empty($_POST['dob'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $dob = $_POST['dob']; 
        try {    
            $query = "UPDATE users SET dob = :dob WHERE id = :id";
            $statement = $db->prepare($query);
            $statement->execute(array(':dob' => $dob, ':id' => $id));

            if($statement->rowCount() == 1) {
                $result .= log_msg("Birth date edited successfully!", "green");
                $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                
                /*==========
                GSheets Storage
                ==========/
                $fields = array(
                    'datetime' => $datetime,
                    'user_id' => $id,
                    'fname' => $_SESSION['fname'],
                    'lname' => $lname,
                    'dob' => $_SESSION['dob'],
                    'phone' => $_SESSION['phone'],
                    'street' => $_SESSION['street'],
                    'city' => $_SESSION['city'],
                    'zip' => $_SESSION['zip'],
                    'email' => $_SESSION['email'],
                    'password' => $_SESSION['password'],
                    'user_photo' => $_SESSION['photo_loc'],
                    "verified" => $_SESSION['verified'],
                    "reset_link" => $reset_link,
                    "edited" => "lname",
                );
                
                $curl_obj = curl_init($sheets_url);
                curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                $headers = array(
                    "Content-Type: application/json",
                    "Access-Control-Allow-Origin: *",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                
                $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                //for debug only!
                //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                
                $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                
                curl_close($curl_obj); // terminates curl 
                
                //var_dump($curl_reply);
                //echo "";
                //print_r($curl_reply);
                $array = json_decode($curl_reply); // converts curl response to array from JSON
                
                /*==========
                End GSheets Storage
                ==========*/

                /*==========
                JSON Storage
                Thanks: thedigitalcraft.com
                ==========*/
                $file_path = $users_json;  // file is be created manually to avoid permission issues
                if (file_exists($file_path)) {
                    $js_data = file_get_contents($file_path);
                } else {
                    file_put_contents($file_path, ''); //creates file if non-existent
                    $js_data = file_get_contents($file_path);
                }
                $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                
                if (is_array($js_array)) { // error handling if no data
                    krsort($js_array); // reverse key sort lib function
                    $js_length = sizeof($js_array);
                } else {
                    //echo("<script>console.log('js_array is empty');</script>");
                }
                
                $js_array[$datetime]['time'] = $datetime;
                $js_array[$datetime]['user_id'] = $id;
                $js_array[$datetime]['fname'] = $_SESSION['fname'];
                $js_array[$datetime]['lname'] = $_SESSION['lname'];
                $js_array[$datetime]['dob'] = $dob;
                $js_array[$datetime]['phone'] = $_SESSION['phone'];
                $js_array[$datetime]['street'] = $_SESSION['street'];
                $js_array[$datetime]['city'] = $_SESSION['city'];
                $js_array[$datetime]['zip'] = $_SESSION['zip'];
                $js_array[$datetime]['email'] = $_SESSION['email'];
                $js_array[$datetime]['password'] = $_SESSION['password'];
                $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                $js_array[$datetime]['verified'] = $_SESSION['verified'];
                $js_array[$datetime]['reset_link'] = $reset_link;
                $js_array[$datetime]['edited'] = "lname";

                save($js_array, $file_path); // save func in utils
                /*==========
                End JSON Storage
                ==========*/

                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'success',
                    title: 'Edit(s) saved! Logout to view changes.',
                    showConfirmButton: true
                }).then(function() {
                    window.location.href = 'account.php';
                });
                </script>";
            } else {
                $result = log_msg("Error, duplicate or invalid entry", "red");
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'No changes made!',
                    showConfirmButton: true
                });
                </script>";
            }
        } catch (PDOException $ex) {
            //$result = log_msg("Error: ".$ex->getMessage());

            // trigger alert
            echo "<script type='text/javascript'>
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'OOPS!',
                text: 'An error occured, please try again.',
                showConfirmButton: true
            });
            </script>";
        }  
    }
    /*==========
     Phone
    ============*/
    if (!empty($_POST['phone'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $form_err = array();
        //$reqd_lenghts = array('email' => 2);
        //$form_err = array_merge($form_err, field_len_check($reqd_lenghts));

        $phone = $_POST['country_code']."-".$_POST['phone'];
        
        if (empty($form_err)) {
            // processes form if no errors found    
            try {    
                $query = "UPDATE users SET phone = :phone WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':phone' => $phone, ':id' => $id));

                if($statement->rowCount() == 1) {
                    $result .= log_msg("Phone edit successful!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                    
                    /*==========
                    GSheets Storage
                    ==========/
                    $fields = array(
                        'datetime' => $datetime,
                        'user_id' => $id,
                        'fname' => $_SESSION['fname'],
                        'lname' => $_SESSION['lname'],
                        'dob' => $_SESSION['dob'],
                        'phone' => $phone,
                        'street' => $_SESSION['street'],
                        'city' => $_SESSION['city'],
                        'zip' => $_SESSION['zip'],
                        'email' => $_SESSION['email'],
                        'password' => $_SESSION['password'],
                        'user_photo' => $_SESSION['photo_loc'],
                        "verified" => $_SESSION['verified'],
                        "reset_link" => $reset_link,
                        "edited" => "phone",
                    );
                    
                    $curl_obj = curl_init($sheets_url);
                    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                    $headers = array(
                        "Content-Type: application/json",
                        "Access-Control-Allow-Origin: *",
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                    
                    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                    //for debug only!
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    
                    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                    
                    curl_close($curl_obj); // terminates curl 
                    
                    //var_dump($curl_reply);
                    //echo "";
                    //print_r($curl_reply);
                    $array = json_decode($curl_reply); // converts curl response to array from JSON
                    
                    /*==========
                    End GSheets Storage
                    ==========*/

                    /*==========
                    JSON Storage
                    Thanks: thedigitalcraft.com
                    ==========*/
                    $file_path = $users_json;  // file is be created manually to avoid permission issues
                    if (file_exists($file_path)) {
                        $js_data = file_get_contents($file_path);
                    } else {
                        file_put_contents($file_path, ''); //creates file if non-existent
                        $js_data = file_get_contents($file_path);
                    }
                    $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                    
                    if (is_array($js_array)) { // error handling if no data
                        krsort($js_array); // reverse key sort lib function
                        $js_length = sizeof($js_array);
                    } else {
                        //echo("<script>console.log('js_array is empty');</script>");
                    }
                    
                    $js_array[$datetime]['time'] = $datetime;
                    $js_array[$datetime]['user_id'] = $id;
                    $js_array[$datetime]['fname'] = $_SESSION['fname'];
                    $js_array[$datetime]['lname'] = $_SESSION['lname'];
                    $js_array[$datetime]['dob'] = $_SESSION['dob'];
                    $js_array[$datetime]['phone'] = $phone;
                    $js_array[$datetime]['street'] = $_SESSION['street'];
                    $js_array[$datetime]['city'] = $_SESSION['city'];
                    $js_array[$datetime]['zip'] = $_SESSION['zip'];
                    $js_array[$datetime]['email'] = $_SESSION['email'];
                    $js_array[$datetime]['password'] = $_SESSION['password'];
                    $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                    $js_array[$datetime]['verified'] = $_SESSION['verified'];
                    $js_array[$datetime]['reset_link'] = $reset_link;
                    $js_array[$datetime]['edited'] = "phone";

                    save($js_array, $file_path); // save func in utils
                    /*==========
                    End JSON Storage
                    ==========*/

                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Edit(s) saved! Logout to view changes.',
                        showConfirmButton: true
                    }).then(function() {
                        window.location.href = 'account.php';
                    });
                    </script>";
                } else {
                    $result = log_msg("Error, duplicate or invalid entry", "red");
                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'OOPS!',
                        text: 'No changes made!',
                        showConfirmButton: true
                    });
                    </script>";
                }
            } catch (PDOException $ex) {
                //$result = log_msg("Error: ".$ex->getMessage());
    
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'Duplicate phone number or file error, please try again.',
                    showConfirmButton: true
                });
                </script>";
            }    
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                //var_dump($form_err);
                // $result = "<p style='color: red;'> Error:</p>";
                // $result .= "<ul style='color: red;'>";
                
                // foreach($form_err as $err) {
                //    $result .= "<li> no {$err} </li>";
                // }
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    }
    /*==========
     Street
    ============*/
    if (!empty($_POST['street'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $form_err = array();
        //$reqd_lenghts = array('email' => 2);
        //$form_err = array_merge($form_err, field_len_check($reqd_lenghts));

        $street = $_POST['street'];
        
        if (empty($form_err)) {
            // processes form if no errors found    
            try {    
                $query = "UPDATE users SET street = :street WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':street' => $street, ':id' => $id));

                if($statement->rowCount() == 1) {
                    $result .= log_msg("Street edited successfully!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                    
                    /*==========
                    GSheets Storage
                    ==========/
                    $fields = array(
                        'datetime' => $datetime,
                        'user_id' => $id,
                        'fname' => $_SESSION['fname'],
                        'lname' => $_SESSION['lname'],
                        'dob' => $_SESSION['dob'],
                        'phone' => $_SESSION['phone'],
                        'street' => $street,
                        'city' => $_SESSION['city'],
                        'zip' => $_SESSION['zip'],
                        'email' => $_SESSION['email'],
                        'password' => $_SESSION['password'],
                        'user_photo' => $_SESSION['photo_loc'],
                        "verified" => $_SESSION['verified'],
                        "reset_link" => $reset_link,
                        "edited" => "street",
                    );
                    
                    $curl_obj = curl_init($sheets_url);
                    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                    $headers = array(
                        "Content-Type: application/json",
                        "Access-Control-Allow-Origin: *",
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                    
                    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                    //for debug only!
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    
                    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                    
                    curl_close($curl_obj); // terminates curl 
                    
                    //var_dump($curl_reply);
                    //echo "";
                    //print_r($curl_reply);
                    $array = json_decode($curl_reply); // converts curl response to array from JSON
                    
                    /*==========
                    End GSheets Storage
                    ==========*/

                    /*==========
                    JSON Storage
                    Thanks: thedigitalcraft.com
                    ==========*/
                    $file_path = $users_json;  // file is be created manually to avoid permission issues
                    if (file_exists($file_path)) {
                        $js_data = file_get_contents($file_path);
                    } else {
                        file_put_contents($file_path, ''); //creates file if non-existent
                        $js_data = file_get_contents($file_path);
                    }
                    $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                    
                    if (is_array($js_array)) { // error handling if no data
                        krsort($js_array); // reverse key sort lib function
                        $js_length = sizeof($js_array);
                    } else {
                        //echo("<script>console.log('js_array is empty');</script>");
                    }
                    
                    $js_array[$datetime]['time'] = $datetime;
                    $js_array[$datetime]['user_id'] = $id;
                    $js_array[$datetime]['fname'] = $_SESSION['fname'];
                    $js_array[$datetime]['lname'] = $_SESSION['lname'];
                    $js_array[$datetime]['dob'] = $_SESSION['dob'];
                    $js_array[$datetime]['phone'] = $_SESSION['phone'];
                    $js_array[$datetime]['street'] = $street;
                    $js_array[$datetime]['city'] = $_SESSION['city'];
                    $js_array[$datetime]['zip'] = $_SESSION['zip'];
                    $js_array[$datetime]['email'] = $_SESSION['email'];
                    $js_array[$datetime]['password'] = $_SESSION['password'];
                    $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                    $js_array[$datetime]['verified'] = $_SESSION['verified'];
                    $js_array[$datetime]['reset_link'] = $reset_link;
                    $js_array[$datetime]['edited'] = "street";

                    save($js_array, $file_path); // save func in utils
                    /*==========
                    End JSON Storage
                    ==========*/

                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Edit(s) saved! Logout to view changes.',
                        showConfirmButton: true
                    }).then(function() {
                        window.location.href = 'account.php';
                    });
                    </script>";
                } else {
                    $result = log_msg("Error, duplicate or invalid entry", "red");
                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'OOPS!',
                        text: 'No changes made!',
                        showConfirmButton: true
                    });
                    </script>";
                }
            } catch (PDOException $ex) {
                //$result = log_msg("Error: ".$ex->getMessage());
    
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'Duplicate phone number or file error, please try again.',
                    showConfirmButton: true
                });
                </script>";
            }    
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                //var_dump($form_err);
                // $result = "<p style='color: red;'> Error:</p>";
                // $result .= "<ul style='color: red;'>";
                
                // foreach($form_err as $err) {
                //    $result .= "<li> no {$err} </li>";
                // }
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    }
    /*==========
     City
    ============*/
    if (!empty($_POST['city'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $form_err = array();
        //$reqd_lenghts = array('email' => 2);
        //$form_err = array_merge($form_err, field_len_check($reqd_lenghts));

        $city = $_POST['city'];
        
        if (empty($form_err)) {
            // processes form if no errors found    
            try {    
                $query = "UPDATE users SET city = :city WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':city' => $city, ':id' => $id));

                if($statement->rowCount() == 1) {
                    $result .= log_msg("City edited successfully!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                    
                    /*==========
                    GSheets Storage
                    ==========/
                    $fields = array(
                        'datetime' => $datetime,
                        'user_id' => $id,
                        'fname' => $_SESSION['fname'],
                        'lname' => $_SESSION['lname'],
                        'dob' => $_SESSION['dob'],
                        'phone' => $_SESSION['phone'],
                        'street' => $_SESSION['street'],
                        'city' => $city,
                        'zip' => $_SESSION['zip'],
                        'email' => $_SESSION['email'],
                        'password' => $_SESSION['password'],
                        'user_photo' => $_SESSION['photo_loc'],
                        "verified" => $_SESSION['verified'],
                        "reset_link" => $reset_link,
                        "edited" => "city",
                    );
                    
                    $curl_obj = curl_init($sheets_url);
                    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                    $headers = array(
                        "Content-Type: application/json",
                        "Access-Control-Allow-Origin: *",
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                    
                    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                    //for debug only!
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    
                    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                    
                    curl_close($curl_obj); // terminates curl 
                    
                    //var_dump($curl_reply);
                    //echo "";
                    //print_r($curl_reply);
                    $array = json_decode($curl_reply); // converts curl response to array from JSON
                    
                    /*==========
                    End GSheets Storage
                    ==========*/

                    /*==========
                    JSON Storage
                    Thanks: thedigitalcraft.com
                    ==========*/
                    $file_path = $users_json;  // file is be created manually to avoid permission issues
                    if (file_exists($file_path)) {
                        $js_data = file_get_contents($file_path);
                    } else {
                        file_put_contents($file_path, ''); //creates file if non-existent
                        $js_data = file_get_contents($file_path);
                    }
                    $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                    
                    if (is_array($js_array)) { // error handling if no data
                        krsort($js_array); // reverse key sort lib function
                        $js_length = sizeof($js_array);
                    } else {
                        //echo("<script>console.log('js_array is empty');</script>");
                    }
                    
                    $js_array[$datetime]['time'] = $datetime;
                    $js_array[$datetime]['user_id'] = $id;
                    $js_array[$datetime]['fname'] = $_SESSION['fname'];
                    $js_array[$datetime]['lname'] = $_SESSION['lname'];
                    $js_array[$datetime]['dob'] = $_SESSION['dob'];
                    $js_array[$datetime]['phone'] = $_SESSION['phone'];
                    $js_array[$datetime]['street'] = $_SESSION['street'];
                    $js_array[$datetime]['city'] = $city;
                    $js_array[$datetime]['zip'] = $_SESSION['zip'];
                    $js_array[$datetime]['email'] = $_SESSION['email'];
                    $js_array[$datetime]['password'] = $_SESSION['password'];
                    $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                    $js_array[$datetime]['verified'] = $_SESSION['verified'];
                    $js_array[$datetime]['reset_link'] = $reset_link;
                    $js_array[$datetime]['edited'] = "city";

                    save($js_array, $file_path); // save func in utils
                    /*==========
                    End JSON Storage
                    ==========*/

                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Edit(s) saved! Logout to view changes.',
                        showConfirmButton: true
                    }).then(function() {
                        window.location.href = 'account.php';
                    });
                    </script>";
                } else {
                    $result = log_msg("Error, duplicate or invalid entry", "red");
                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'OOPS!',
                        text: 'No changes made!',
                        showConfirmButton: true
                    });
                    </script>";
                }
            } catch (PDOException $ex) {
                //$result = log_msg("Error: ".$ex->getMessage());
    
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'Duplicate phone number or file error, please try again.',
                    showConfirmButton: true
                });
                </script>";
            }    
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                //var_dump($form_err);
                // $result = "<p style='color: red;'> Error:</p>";
                // $result .= "<ul style='color: red;'>";
                
                // foreach($form_err as $err) {
                //    $result .= "<li> no {$err} </li>";
                // }
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    }
    /*==========
     Zip
    ============*/
    if (!empty($_POST['zip'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 

        $form_err = array();
        //$reqd_lenghts = array('email' => 2);
        //$form_err = array_merge($form_err, field_len_check($reqd_lenghts));

        $zip = $_POST['zip'];
        
        if (empty($form_err)) {
            // processes form if no errors found    
            try {    
                $query = "UPDATE users SET zip = :zip WHERE id = :id";
                $statement = $db->prepare($query);
                $statement->execute(array(':zip' => $zip, ':id' => $id));

                if($statement->rowCount() == 1) {
                    $result .= log_msg("Postal code edited successfully!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
                    
                    /*==========
                    GSheets Storage
                    ==========/
                    $fields = array(
                        'datetime' => $datetime,
                        'user_id' => $id,
                        'fname' => $_SESSION['fname'],
                        'lname' => $_SESSION['lname'],
                        'dob' => $_SESSION['dob'],
                        'phone' => $_SESSION['phone'],
                        'street' => $_SESSION['street'],
                        'city' => $_SESSION['city'],
                        'zip' => $zip,
                        'email' => $_SESSION['email'],
                        'password' => $_SESSION['password'],
                        'user_photo' => $_SESSION['photo_loc'],
                        "verified" => $_SESSION['verified'],
                        "reset_link" => $reset_link,
                        "edited" => "zip",
                    );
                    
                    $curl_obj = curl_init($sheets_url);
                    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                    $headers = array(
                        "Content-Type: application/json",
                        "Access-Control-Allow-Origin: *",
                    );
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                    
                    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                    //for debug only!
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                    
                    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                    
                    curl_close($curl_obj); // terminates curl 
                    
                    //var_dump($curl_reply);
                    //echo "";
                    //print_r($curl_reply);
                    $array = json_decode($curl_reply); // converts curl response to array from JSON
                    
                    /*==========
                    End GSheets Storage
                    ==========*/

                    /*==========
                    JSON Storage
                    Thanks: thedigitalcraft.com
                    ==========*/
                    $file_path = $users_json;  // file is be created manually to avoid permission issues
                    if (file_exists($file_path)) {
                        $js_data = file_get_contents($file_path);
                    } else {
                        file_put_contents($file_path, ''); //creates file if non-existent
                        $js_data = file_get_contents($file_path);
                    }
                    $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                    
                    if (is_array($js_array)) { // error handling if no data
                        krsort($js_array); // reverse key sort lib function
                        $js_length = sizeof($js_array);
                    } else {
                        //echo("<script>console.log('js_array is empty');</script>");
                    }
                    
                    $js_array[$datetime]['time'] = $datetime;
                    $js_array[$datetime]['user_id'] = $id;
                    $js_array[$datetime]['fname'] = $_SESSION['fname'];
                    $js_array[$datetime]['lname'] = $_SESSION['lname'];
                    $js_array[$datetime]['dob'] = $_SESSION['dob'];
                    $js_array[$datetime]['phone'] = $_SESSION['phone'];
                    $js_array[$datetime]['street'] = $_SESSION['street'];
                    $js_array[$datetime]['city'] = $_SESSION['city'];
                    $js_array[$datetime]['zip'] = $zip;
                    $js_array[$datetime]['email'] = $_SESSION['email'];
                    $js_array[$datetime]['password'] = $_SESSION['password'];
                    $js_array[$datetime]['user_photo'] = $_SESSION['photo_loc'];
                    $js_array[$datetime]['verified'] = $_SESSION['verified'];
                    $js_array[$datetime]['reset_link'] = $reset_link;
                    $js_array[$datetime]['edited'] = "zip";

                    save($js_array, $file_path); // save func in utils
                    /*==========
                    End JSON Storage
                    ==========*/

                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'success',
                        title: 'Edit(s) saved! Logout to view changes.',
                        showConfirmButton: true
                    }).then(function() {
                        window.location.href = 'account.php';
                    });
                    </script>";
                } else {
                    $result = log_msg("Error, duplicate or invalid entry", "red");
                    // trigger alert
                    echo "<script type='text/javascript'>
                    Swal.fire({
                        position: 'center',
                        icon: 'error',
                        title: 'OOPS!',
                        text: 'No changes made!',
                        showConfirmButton: true
                    });
                    </script>";
                }
            } catch (PDOException $ex) {
                //$result = log_msg("Error: ".$ex->getMessage());
    
                // trigger alert
                echo "<script type='text/javascript'>
                Swal.fire({
                    position: 'center',
                    icon: 'error',
                    title: 'OOPS!',
                    text: 'Duplicate phone number or file error, please try again.',
                    showConfirmButton: true
                });
                </script>";
            }    
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                //var_dump($form_err);
                // $result = "<p style='color: red;'> Error:</p>";
                // $result .= "<ul style='color: red;'>";
                
                // foreach($form_err as $err) {
                //    $result .= "<li> no {$err} </li>";
                // }
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    }
    /*==========
     Photo
    ============*/  
    if (!empty($_FILES['photo']['name'])) {
        if (isset($_GET['id'])) {
            $url_encoded_id = $_GET['id'];
            $url_decoded_id = base64_decode($url_encoded_id);
            $url_id_arr = explode("happygoid", $url_decoded_id);
            $id = $url_id_arr[1];
            //echo $id;
        } 
        
        //$file_path = addslashes(file_get_contents($usr_photo));
        $form_err = array();
        $email = $_SESSION['email'];

        $usr_img = $_FILES['photo']; // gives array of file attributes (name, size, etc)
        $img_name = $usr_img['name']; 
        $img_parts = explode(".", $img_name);
        $img_ext = strtolower(end($img_parts));
        $email_chars = array("@", ".");
        $email_name = str_replace($email_chars, "_", $email);
        $creation_time = str_replace(".", "_", microtime(true));
        $sec_img_name =  "photo_".$id."_".$creation_time.".".$img_ext;

        //$img_path = "resources/uploads/profile/".$sec_img_name;
        //$img_dir = "resources/uploads/".$fname."_".$lname."_".$creation_time."/";
        //"resources/uploads/user".$id.".jpg";
        /*
        if (!mkdir($img_dir)) {
            // DIRECTORY CREATION ERROR
            echo "<script type='text/javascript'>
            Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'OOPS!',
                text: 'Folder creation error, please contact us.',
                showConfirmButton: true,
                });
                </script>";
        }
        */
        
        // performs valid img and initial upload check
        $form_err = array_merge($form_err, valid_img($usr_img));

        if (empty($form_err)) {
            // processes form if no errors found 

            // UPLOADS FILE TO SERVER (store path DB BEFORE upload)

            // ASSIGNS A UNIQUE NAME & LOC TO IMG (links to user) DEFAULT PHOTO_ID = USER_ID (changes after upload)
            // generating the unique id BEFORE inserting saves an additional query since we can just store the id first
            // querying for the unique id will thus only be needed after logout

            //$img_path = "resources/uploads/profile/user_".$id."_".$photo_id.".".$img_ext;
            $img_path = "resources/uploads/profile/".$sec_img_name;
            $query_path = addslashes($img_path);

            try {
                // INSERTS IMG INTO IMG TABLE (backticks needed for MYSQL reserved words!)
                $query = "INSERT INTO user_photos (usr_id, photo_loc) VALUES (:usr_id, :photo_loc)";
                $statement = $db->prepare($query);
                $statement->execute(array(':usr_id' => $id, ':photo_loc' => $query_path));
                
                if ($statement->rowCount() < 1) {
                    $result = log_msg("Error: photo not set, please contact us or retry.", "red");
                } else {  // IF INSERT SUCCEEDS, DO NOT PERFORM SELECT QUERY TO USE FETCH!!!      
                          // JUST USE img_path as new photo_loc!
                    $result .= log_msg("Photo edited successfully!", "green");
                    $reset_link = "Hi $fname, your account info was changed at $datetime. Please contact us if this is an error.";
        
                    if(move_uploaded_file($usr_img['tmp_name'], $img_path)){
                        $_SESSION['photo_loc'] = $img_path;

                        /*==========
                        GSheets Storage
                        ==========/
                        $fields = array(
                            'datetime' => $datetime,
                            'user_id' => $id,
                            'fname' => $_SESSION['fname'],
                            'lname' => $_SESSION['lname'],
                            'dob' => $_SESSION['dob'],
                            'phone' => $_SESSION['phone'],
                            'street' => $_SESSION['street'],
                            'city' => $_SESSION['city'],
                            'zip' => $_SESSION['zip'],
                            'email' => $_SESSION['email'],
                            'password' => $_SESSION['password'],
                            'user_photo' => $img_path,
                            "verified" => $_SESSION['verified'],
                            "reset_link" => $reset_link,
                            "edited" => "photo",
                        );
                        
                        $curl_obj = curl_init($sheets_url);
                        curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
                        curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                        curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                        curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                        $headers = array(
                            "Content-Type: application/json",
                            "Access-Control-Allow-Origin: *",
                        );
                        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                        curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                        
                        $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                        $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                        //for debug only!
                        //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                        //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                        
                        $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                        
                        curl_close($curl_obj); // terminates curl 
                        
                        //var_dump($curl_reply);
                        //echo "";
                        //print_r($curl_reply);
                        $array = json_decode($curl_reply); // converts curl response to array from JSON
                        
                        /==========
                        End GSheets Storage
                        ==========*/

                        /*==========
                        JSON Storage
                        Thanks: thedigitalcraft.com
                        ==========*/
                        $file_path = $users_json;  // file is be created manually to avoid permission issues
                        if (file_exists($file_path)) {
                            $js_data = file_get_contents($file_path);
                        } else {
                            file_put_contents($file_path, ''); //creates file if non-existent
                            $js_data = file_get_contents($file_path);
                        }
                        $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                        
                        if (is_array($js_array)) { // error handling if no data
                            krsort($js_array); // reverse key sort lib function
                            $js_length = sizeof($js_array);
                        } else {
                            //echo("<script>console.log('js_array is empty');</script>");
                        }
                        
                        $js_array[$datetime]['time'] = $datetime;
                        $js_array[$datetime]['user_id'] = $id;
                        $js_array[$datetime]['fname'] = $_SESSION['fname'];
                        $js_array[$datetime]['lname'] = $_SESSION['lname'];
                        $js_array[$datetime]['dob'] = $_SESSION['dob'];
                        $js_array[$datetime]['phone'] = $_SESSION['phone'];
                        $js_array[$datetime]['street'] = $_SESSION['street'];
                        $js_array[$datetime]['city'] = $_SESSION['city'];
                        $js_array[$datetime]['zip'] = $_SESSION['zip'];
                        $js_array[$datetime]['email'] = $_SESSION['email'];
                        $js_array[$datetime]['password'] = $_SESSION['password'];
                        $js_array[$datetime]['user_photo'] = $img_path;
                        $js_array[$datetime]['verified'] = $_SESSION['verified'];
                        $js_array[$datetime]['reset_link'] = $reset_link;
                        $js_array[$datetime]['edited'] = "photo";

                        save($js_array, $file_path); // save func in utils
                        /*==========
                        End JSON Storage
                        ==========*/

                        // trigger success alert
                        echo "<script type='text/javascript'>
                        Swal.fire({
                            position: 'center',
                            icon: 'success',
                            title: 'Upload successful! Logout to view changes.',
                            showConfirmButton: true
                        }).then(function() {
                            window.location.href = 'account.php';
                        });
                        </script>";            
                    } else {
                        // THROWS FILE UPLOAD (PATH CREATION/INSERTION) ERROR
                        echo "<script type='text/javascript'>
                        Swal.fire({
                            position: 'center',
                            icon: 'error',
                            title: 'File upload error!',
                            text: 'Please contact us or try again.',
                            showConfirmButton: true
                        });
                        </script>";
                    }    
                }
            } catch (PDOException $ex) {
                $result = log_msg("Error: ".$ex->getMessage());
                // THROWS DATABASE INSERT ERROR
                echo "<script type='text/javascript'>
                Swal.fire({
                position: 'center',
                icon: 'error',
                title: 'OOPS!',
                text: 'Database error occured, please try again.',
                showConfirmButton: true
                });
                </script>";
            }
        } else { 
            // displays each error as li if found (in PHP the .= operator appends to a var)
            if(count($form_err) >= 1){
                $result = log_msg("Error count: ".count($form_err), "red");
            }
        }
    } 

    /*==========
     Write final JSON to GSheets
    ============*/  
    
    $fields = array_flatten($js_array);
    $fields['user_ip'] = ""; // needed for script control
    
    $curl_obj = curl_init($sheets_url);
    curl_setopt($curl_obj, CURLOPT_URL, $sheets_url);
    curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
    $headers = array(
        "Content-Type: application/json",
        "Access-Control-Allow-Origin: *",
    );
    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
    
    $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
    $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
    //for debug only!
    //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
    //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
    
    $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
    
    curl_close($curl_obj); // terminates curl 

    $array = json_decode($curl_reply); // converts curl response to array from JSON
    
    /*==========
     No changes
    ============*/    
    if (empty($_POST['fname']) && empty($_POST['lname']) && empty($_POST['phone']) && empty($_FILES['photo']['name']) && empty($_POST['street']) && empty($_POST['city']) && empty($_POST['zip']) ) {
        $result = log_msg("No changes made", "green");
        // trigger alert
        echo "<script type='text/javascript'>
        Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'OK!',
            text: 'No changes made?',
            showConfirmButton: true
        }).then(function() {
            window.location.href = 'account.php';
        });
        </script>";
    } 

    if(count($form_err) >= 1){
        //var_dump($form_err);
        // $result = "<p style='color: red;'> Error:</p>";
        // $result .= "<ul style='color: red;'>";
        
        // foreach($form_err as $err) {
        //    $result .= "<li> no {$err} </li>";
        // }
        $result = log_msg("Error count: ".count($form_err), "red");
    }

    // store inputs
    //$email = $_POST['email'];
    //$fname = $_POST['fname'];
    //$lname = $_POST['lname']; 
} else {
    // if no id and email session vars are not set and edit btn is not yet clicked
} 

?>