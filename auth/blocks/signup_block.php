
<?php
define('scsn', TRUE);

include_once('../../../db_conn/auth_db.php'); // path is diff bc ajax
include_once('../resources/utils.php');

if(defined('users_json')) {
   // echo('users_json found!<br>');
}

$registered = false;
// form validation on button click
$file_path = "../".$users_json;  // signup uses ajax so rel loc is from file loc, file is be created manually to avoid permission issues
if (file_exists($file_path)) {
    $js_data = file_get_contents($file_path);
    //echo "file found: ".$file_path;
} else {
    file_put_contents($file_path, ''); //creates file if non-existent
    $js_data = file_get_contents($file_path);
    //echo "file not found";
}

if (isset($_POST['fname']) && isset($_POST['dob']) && isset($_POST['phone']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['conf_password'])) {
    
    // store inputs
    $id = 0; // ID will be incremented after submit  
    //var_dump($_POST);
    $err_list = array();
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $dob = $_POST['dob'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];
    $street = $_POST['street'];
    $city = $_POST['city'];
    $zip = $_POST['zip'];
    $reset_link = "";
    
    $password = $_POST['password'];
    $conf_password = $_POST['conf_password'];    

    $sec_passwd = password_hash($password, PASSWORD_BCRYPT);

    // initializes array to store any error msgs
    $form_err = array();
    $result = "";

    // validates form
    $required_fields = array('fname', 'email', 'password');

    // performs empty checks and merges with orig array
    $form_err = array_merge($form_err, form_empty_check($required_fields));
    
    if (!empty(form_empty_check($required_fields))) {
        $err_list[] = array("Error" => "Form is empty.");
    }
    
    
    // performs min length check
    $reqd_lenghts = array('fname' => 1, 'email' => 2, 'password' => 5); 
    $form_err = array_merge($form_err, field_len_check($reqd_lenghts));

    if (!empty(field_len_check($reqd_lenghts))) {
        $err_list[] = array("Error" => "First name, email, or password too short.");
    }

    // performs email validation
    $form_err = array_merge($form_err, email_check($_POST));

    if (!empty(email_check($_POST))) {
        $err_list[] = array("Error" => "Invalid email.");
    }

    // performs passwords match check
    $passwords = array($_POST['password'] => $_POST['conf_password']);
    $form_err = array_merge($form_err, password_match($passwords));

    if (!empty(password_match($passwords))) {
        $err_list[] = array("Error" => "Passwords must match.");
    }

    //if (isset($_POST['email']))    
    
    //$file_path = addslashes(file_get_contents($usr_photo));


    // perform duplicate email check
    if (dup_email($email, $db)) {
        $err_list[] = array("Error" => "Email already exists, please login or retry.");

        $result = log_msg("Email already exists. Please login or retry.");
        /*/ trigger alert
        echo "<script type='text/javascript'>
        Swal.fire({
            position: 'center',
            icon: 'error',
            title: 'OOPS!',
            text: '$email already exists, please login or try again.',
            showConfirmButton: true,
          });
          </script>"; */

    // store data if no errors found        
    } else if (empty($form_err)) {

        try{
            /*==========
            SQL Storage
            ==========*/
            $sql_ins = "INSERT INTO users (username, fname, lname, dob, phone, email, street, city, zip, password, reset_link, join_date) 
                        VALUES (:username, :fname, :lname, :dob, :phone, :email, :street, :city, :zip, :password, :reset_link, now())";

            $statement = $db->prepare($sql_ins);
            $statement->execute(array(':username' => $email, ':fname' => $fname, ':lname' => $lname, ':dob' => $dob, ':phone' => $phone, ':email' => $email, ':street' => $street, ':city' => $city, ':zip' => $zip, ':password' => $sec_passwd, ':reset_link' => $reset_link));
            
            $sec_passwd = password_hash($password, PASSWORD_BCRYPT);

            if($statement->rowCount() == 1) {
                $id = $db->lastInsertId();
                $enc_id = encryptString($id, "user_verification");
                $sec_user_id = base64_encode("i_am_the_greenest_geek{$id}");
                $email_body = "Hi $fname, please visit this page to be verified: https://alocs.com/auth/verify.php?id=".$sec_user_id;


                $img_path = "resources/uploads/profile/def_profile_pic2.png";

                /*$sql_sel = "SELECT * FROM users 
                            WHERE email = :email";

                $statement = $db->prepare($sql_sel);
                $statement->execute(array(':email' => $email));

                $row = $statement->fetch();
                $id = $row['id'];
                $join_date = $row['join_date'];*/
                
                $sql_ins = "INSERT INTO user_photos (usr_id, photo_loc) 
                            VALUES (:usr_id, :photo_loc)";

                $statement = $db->prepare($sql_ins);
                $statement->execute(array(':usr_id' => $id, ':photo_loc' => $img_path));


                /*==========
                GSheets Storage
                ==========*/
                $url = 'https://script.google.com/macros/s/AKfycbxb3DH4VNL585thGxuLhKqLSchW6lL1xm4smKxd8SQ_7sYBOyeUi1lMUWjLJjNq_LZxyQ/exec';
                
                $fields = array(
                    'datetime' => $datetime, // datetime from database.php
                    'user_ip' => "",  // used for script control
                    'user_id' => $id,
                    'fname' => $fname,
                    'lname' => $lname,
                    'dob' => $dob,
                    'phone' => $phone,
                    'email' => $email,
                    'street' => $street,
                    'city' => $city,
                    'zip' => $zip,
                    'password' => $sec_passwd,
                    'user_photo' => $img_path,
                    "verified" => 0,
                    "reset_link" => $email_body,
                    "edited" => "all",
                );
                
                $curl_obj = curl_init($url);
                curl_setopt($curl_obj, CURLOPT_URL, $url);
                curl_setopt($curl_obj, CURLOPT_POST, count($fields)); // sets the num of fields param
                curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($curl_obj, CURLOPT_POSTFIELDS, $fields); // sets the field contents param
                $headers = array(
                    "Content-Type: application/json",
                    "Access-Control-Allow-Origin: *",
                );
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                curl_setopt($curl_obj, CURLOPT_RETURNTRANSFER, True); // sets the return result param
                
                $GLOBALS['status_code_http'] = curl_getinfo($curl_obj, CURLINFO_HTTP_CODE); // sets http global to curl return value for http code
                $GLOBALS['status_code_https'] = curl_getinfo($curl_obj, CURLINFO_HTTPS_CODE);
                //for debug only!
                //curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
                //curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
                
                $curl_reply = curl_exec($curl_obj); // executes curl connection and stores reply values in var
                
                curl_close($curl_obj); // terminates curl 
                
                //var_dump($curl_reply);
                //echo "";
                //print_r($curl_reply);
                $array = json_decode($curl_reply); // converts curl response to array from JSON
                
                /*==========
                End GSheets Storage
                ==========*/

                /*==========
                JSON Storage
                Thanks: thedigitalcraft.com
                ==========*/
                $file_path = "../".$users_json;  // signup uses ajax so rel loc is from file loc, file is be created manually to avoid permission issues
                if (file_exists($file_path)) {
                    $js_data = file_get_contents($file_path);
                } else {
                    file_put_contents($file_path, ''); //creates file if non-existent
                    $js_data = file_get_contents($file_path);
                }
                $js_array = json_decode($js_data, 1); // 1 for ASSOC = TRUE, to revert in live server, just issue git stash (this should be the only changed file)
                
                if (is_array($js_array)) { // error handling if no data
                    krsort($js_array); // reverse key sort lib function
                    $js_length = sizeof($js_array);
                } else {
                    //echo("<script>console.log('js_array is empty');</script>");
                }
                
                $js_array[$datetime]['datetime'] = $datetime; // datetime from database.php
                $js_array[$datetime]['user_id'] = $id;
                $js_array[$datetime]['fname'] = $fname;
                $js_array[$datetime]['lname'] = $lname;
                $js_array[$datetime]['dob'] = $dob;
                $js_array[$datetime]['phone'] = $phone;
                $js_array[$datetime]['email'] = $email;
                $js_array[$datetime]['street'] = $street;
                $js_array[$datetime]['city'] = $city;
                $js_array[$datetime]['zip'] = $zip;
                $js_array[$datetime]['password'] = $sec_passwd;
                $js_array[$datetime]['user_photo'] = $img_path;
                $js_array[$datetime]['verified'] = 0;
                $js_array[$datetime]['reset_link'] = $email_body;
                $js_array[$datetime]['edited'] = "all";

                save($js_array, $file_path); // save func in utils
                /*==========
                End JSON Storage
                ==========*/
                $result = log_msg("Activation email sent!", "green");
                $registered = true;
            }
        } catch (PDOException $ex) {
            $err_list[] = array("Error" => "SQL Query Error.");
            $result = log_msg("Error: ".$ex->getMessage());
        }
        /*==========
        End SQL Storage
        ==========*/
    } else { 
        // displays each error as li if found (in PHP the .= operator appends to a var)
        if(count($form_err) >= 1){
            // $result = "<p style='color: red;'> Error:</p>";
            // $result .= "<ul style='color: red;'>";
            
            // foreach($form_err as $err) {
            //    $result .= "<li> no {$err} </li>";
            // }
            $result = log_msg("Error count: ".count($form_err), "red");
            $err_list[] = array("Error count" => count($form_err));
        }
    }
} 
$err_json = json_encode($err_list);
print_r($err_json);
?>
