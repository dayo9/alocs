<?php 
$page_title = "Account";
include_once("blocks/header.php");
include_once("blocks/account_block.php");
$fname = $_SESSION['fname'];
$lname = $_SESSION['lname'];
$dob = $_SESSION['dob'];
$street = $_SESSION['street'];
$city = $_SESSION['city'];
$zip = $_SESSION['zip'];
$join_date = $_SESSION['join_date'];
//echo $_SESSION['login_time'];
?>
<body>
<div class="container">
<section class="col col-lg-7">
    <h2>
    <?php if (isset($_SESSION['fname'])) : ?>
    <?php echo $_SESSION['fname']."'s ";?> Account
    </h2>
    <?php else: ?>
    Invalid Account
    </h2>
    <br>
    <p>You are not logged in. <a href="login.php">Login here</a></p> 
    <?php echo"<script>window.location.replace('http://starcanyon.org');</script>"; endif ?>
    <hr>
    <div class="row col-lg-3">
    <img src="<?php if (isset($acct_img)) echo $acct_img; ?>" class="img img-rounded" style="margin-bottom: 10px; width: 200px;">
    </div>
    
    <table class="table table-bordered table-condensed">
    <tr>
    <th style="width:105px;">First Name:</th>
    <td>
    <?php if(isset($fname)) echo $fname; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Last Name:</th>
    <td>
    <?php if(isset($lname)) echo $lname; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Birth Date:</th>
    <td>
    <?php if(isset($dob)) echo $dob; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Email:</th>
    <td>
    <?php if(isset($email)) echo $email; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Phone:</th>
    <td>
    <?php if(isset($phone)) echo $phone; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Address (street):</th>
    <td>
    <?php if(isset($street)) echo $street; ?>
    </td>
    </tr>
    <tr>
    <th style="...">City:</th>
    <td>
    <?php if(isset($city)) echo $city; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Postal Code:</th>
    <td>
    <?php if(isset($zip)) echo $zip; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Student ID:</th>
    <td>
    <?php if(isset($id)) echo $id; ?>
    </td>
    </tr>
    <tr>
    <th style="...">Edit date:</th>
    <td>
    <?php if(isset($join_date)) echo $join_date; ?>
    </td>
    </tr>
    <tr>
    <th></th>
    <td>
    <a class="pull-right" href="edit_acct.php?id=<?php if(isset($sec_id)) echo $sec_id; ?>">
    <span class="glyphicon glyphicon-edit">
    </span>
    Edit profile
    </a>
    </td>
    </tr>
    </table>
</section>
</div>
</body>
<?php
include_once("blocks/footer.php");
?>