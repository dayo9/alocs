<!-- 
Edited: 07/04/2021
By: David Ayodele

References:
Josh Sorosky - https://codepen.io/joshsorosky/pen/gaaBoB
Ashly Lorenzana - http://www.ashlylorenzana.com
https://codepen.io/sitapson/pen/bEdOab
-->
<?php
define('scsn', TRUE);
/*include_once('../db_conn/conn_db.php'); // db config outside web dir ($db_conn is varname)*/
//include_once("cap"); 
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign-up</title>
    <link href="css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/font-awesome-4.7.0/css/font-awesome.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,700" rel="stylesheet" type="text/css">
    <link href= 'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href= 'https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
    <!-- jQuery load -->
    <script src="js/jquery.min.js">
    </script>
</head>

<body>
<section id="contact_form">
<!-- Sliding form - Adapted from  -->
<div class="cf_container">
<div class="cf_frame">
<form action="" id="contactForm" class="cf_form" method="POST" target="hidden_iframe" name="form" onsubmit="return validateFormOnSubmit(this); event.preventDefault();">
    <h2 class="cf_heading">Account Sign Up</h2>
    <h6 class="cf_heading">Complete this form to create a user account, then check your email for confirmation. </h6>
    <input type="text" name="catcher" id="catcher_id" class="catcher_class" value=""/>

    <div class="input name">
    <i class="fa fa-user"></i>
    <label for="entry_257768866">Name</label>
    <input class="cf_form-styling" type="text" name="name" id="name" required> <!-- placeholder="&#xf007;" -->
    </div>

    <div class="input name">
    <i class="fa fa-phone"></i>
    <label for="entry_301549428">Phone (ex: (555)555-5555)</label>
    <input class="cf_form-styling" type="tel" name="phone" id="phone" required> <!-- placeholder="&#xf095;" -->
    </div>

    <div class="input name">
    <i class="fa fa-at"></i>
    <label for="entry_603272949">Email</label>
    <input class="cf_form-styling" type="email" name="email" id="email" required> <!-- placeholder="@" pattern="[0-9]*" -->
    </div>
    
    <label for="entry_1557960477">Subject</label>
    <select class="cf_form-styling" name="subject" id="subject" placeholder="&#xf017;">
    <option>- Select a subject area -</option>
    <option value="CNA/LNA">CNA/LNA</option>
    <option value="HESI A2">HESI A2</option>
    <option value="Other">Other</option>
    </select>

    <label for="entry_141256357">Course Time</label>
    <select class="cf_form-styling" name="course_time" id="course_time" placeholder="&#xf0f1;">
    <option>- Select a start time -</option>
    <option value="10 AM">10 AM</option>
    <option value="Noon">Noon</option>
    <option value="2 PM">2 PM</option>
    <option value="4 PM">4 PM</option>
    <option value="6 PM">6 PM</option>
    </select>

    <label for="entry_720829911">Course date (MM/DD/YYYY, must be a future Fri or Sat)</label>
    <input class="cf_form-styling" type="date" name="course_date" id="course_date_math" placeholder="&#xf1fd;" required>

    <div class="input msg">
    <i class="fa fa-comment"></i>
    <label for="entry_557222746">Additional info (optional)</label>
    <textarea class="cf_form-styling" name="message" id="msg" rows=5 tabindex=20 placeholder="Anything else you want us to know?"></textarea> <!-- placeholder="&#xf075;" -->
    </div>

    <!-- <label for="checkbox"><span class="ui"></span>Keep me signed in</label> -->
    <center class="cf_bottom">
    <button class="btn-submit btn-animate" type="submit" name="cf_submit" id="ss-submit" tabindex=5>Submit</button>
    <!--
    <div class="btn-animate">
    <a class="btn-CNA_LNA" id="ss-submit">Submit</a>           
    </div> 
    -->
    <br>
    <br>
    <br>
    <br>
    <span style="color: #fff; font: bold normal 14px/80% Arial, Helvetica, sans-serif; line-height: 14px;">To submit, click the checkbox below<br><br></span>
    <br>
    <div class="g-recaptcha recaptcha" id="captchabox">
    </div>
    <span id="status_msg" name="msg">
    </span>
    <br>
    <br>
    <div id="successMessage" style="display: none;"> 
    <br>
    </div>            
    </center>
</form>
</div>
</section>
<br>
<!-- End contact form -->
</body>

<!-- Google recaptcha PHP call script -->
<script type="text/javascript">

function _(id){ return document.getElementById(id); }

var form = _("contactForm");

function reCaptcha() {
    $.get('grecaptcha/org_recaptcha_loc.php'); //cap, ignored/ezy_recaptcha_loc.php, cap will not work locally, htaccess only working live
}
</script>

<!-- Contact form script -->
<script type="text/javascript">
var submitted = false;
var name = '';
var str = '';
var n = -1;
var recaptcha_id1;

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();
//var course_dates = ["2019-01-21", "2019-01-28", "2019-03-04", "2019-03-18", "2019-04-15", "2019-05-06", "2019-06-24", "2019-07-29", "2019-08-12", "2019-09-30", "2019-11-18", "2020-01-06", "2020-02-24", "2020-04-13", "2020-07-20", "2020-09-07"];

if(dd<10) {
    dd = '0'+dd
} 
if(mm<10) {
    mm = '0'+mm
} 

today = yyyy + '-' + mm + '-' + dd;

Date.daysBetween = function( date1, date2 ) {
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;
    
    // Convert back to days and return
    return Math.round(difference_ms/one_day); 
}

var onloadCallback = function() {
// Renders the HTML element with id 'example1' as a reCAPTCHA widget.
// The id of the reCAPTCHA widget is assigned to 'widgetId1'.
// from https://developers.google.com/recaptcha/docs/display#recaptcha_methods

// If error "uncaught promise, null" received, check all scripts for errors, undefined vars, jQuery load, etc!!!

    _("ss-submit").disabled = true;

    recaptcha_id1 = grecaptcha.render(_("captchabox"), {
    'sitekey' : '6LfSSaUZAAAAAOdBsnGmxp9F_MI2yjQyj1Eneu9W', /* live: 6Lc6qIMbAAAAAK1eOBvIE5kYTogg4Nf9MLcOeNc7 or 6Lf8sVsUAAAAAGSeEocTRqoyho51t1Tp5AInt4kh local: 6LfSSaUZAAAAAOdBsnGmxp9F_MI2yjQyj1Eneu9W*/
    'callback' : imNotARobot,
    'theme' : 'light',
    'type' : 'image'
    });
    
    /*Rescaling recaptcha adapted from: https://stackoverflow.com/questions/22991938/overriding-google-recaptcha-css-to-make-it-responsive */
    function rescaleCaptcha(){
        var width = $('.g-recaptcha').parent().width();
        var scale;
        if (width < 302) {
            scale = width / 302;
        } else {
            scale = 1.0; 
        }

        $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
        $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
        $('.g-recaptcha').css('transform-origin', '0 0');
        $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
    }

    rescaleCaptcha();
    $( window ).resize(function() { rescaleCaptcha(); });
}; 


var imNotARobot = function() {
    var date_obj_today = new Date();
    var date_obj_course = new Date(_("course_date_math").value.replace(/-/g, "/"));
    var str_math;

    console.info("Captcha box was clicked");

    str_math = grecaptcha.getResponse(recaptcha_id1);
    console.log(str_math);
    email = $("#email").val();
    name = $("#name").val();
    var fullName = name.split(' '),
        firstName = fullName[0],
        lastName = fullName[fullName.length - 1];

    $.ajax({
        type: 'POST',
        url: 'cap', //cap, ignored/math_form_recaptcha.php, cap will not work locally, htaccess only working live
        data: {uname: name, email: email, 'g-recaptcha-response': str_math}, /*"name=" + name + "&g-recaptcha-response=" + str*/
        success: function(data) {		  			
            console.log(str);
            console.log(data); // data contains data sent as well any echoed vars from called url.
            data_json = JSON.parse(data);
            console.log(data_json);
            console.log(data_json['success']);
            //document.getElementById("status_msg").innerHTML = n;
            //n = str.indexOf("03A");
            //console.log("n = " + n);
            n = data_json['success'] ? 1 : 0;
            console.log(n);
            if (!(date_obj_course.getDay() == 5 || date_obj_course.getDay() == 6) || (date_obj_course < date_obj_today)) {
                _("status_msg").className = "red_msg";
                console.info("Course date slected is " + _("course_date_math").value.replace(/-/g, "/") + ", Weekday: " + date_obj_course.getDay() + ". Today is: " + date_obj_today);
                _("status_msg").innerHTML = "Invalid course date selected: " + _("course_date_math").value.replace(/-/g, "/") + " is not a future Friday or Saturday. Please check your calendar or try another browser (Chrome/Firefox).";
            } else if (data_json['success']) {
                //alert('Probably not a robot');
                console.log("captcha passed");
                $("#status_msg").removeClass();
                $("#alert-msg").removeClass("alert-hide");
                $("#alert-msg").addClass("alert-show");
                $("#status_msg").addClass( "green_msg" );
                _("ss-submit").disabled = false;
                _("status_msg").innerHTML = "Success! Please check your email spelling and submit!";
            } else {
                // Do something if a robot
                _("ss-submit").disabled = true;
            }  	
        }
    })
};        

/*------- 
Contact form validation
--------*/

function myTime(){
    var months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var d = new Date();
    var day = days[d.getDay()];
    var hr = d.getHours();
    var min = d.getMinutes();
    var secs = d.getSeconds();
    if (min < 10) {
        min = "0" + min;
    }
    var ampm = "am";
    if( hr > 12 ) {
        hr -= 12;
        ampm = "pm";
    }
    var date = d.getDate();
    var month = months[d.getMonth()];
    var year = d.getFullYear();

    return hr + ":" + min + ":" + secs + ampm + ", " + day + " " + month + " " + date + ", " + year;
}

function validateFormOnSubmit(contact) { // takes contact var as input for legacy browser support funcs
    var i = _("status_msg");
    var j = _("successMessage");
    
    var formdata = new FormData();
    formdata.append( "name", _("name").value );
    formdata.append( "email", _("email").value );
    formdata.append( "msg", _("msg").value );
    const timeElapsed = Date.now();
    const stamp = new Date(timeElapsed);
    var timestamp = stamp.toUTCString();
    
    name = $("#name").val();
    var fullName = name.split(' '),
        firstName = fullName[0].trim(),
        lastName = fullName[fullName.length - 1].trim();
    phone = _("phone").value;
    email = _("email").value;
    subject = _("subject").value;
    course_time = _("course_time").value;
    course_date = _("course_date_math").value;
    msg = _("msg").value;

    var form = $("#contactForm"); // must use jquery to access serialize function
    var data_ser = form.serialize(); // Seriale form data

    const url = 'https://script.google.com/macros/s/AKfycbzK4DvzBTLC58L2-ijQ-ANh2pwACaMWB8ZJZ6VJ4vk6cb-IcBNCBdPhPa9JD6b9U7L3/exec';
    // 'store' will not work locally, htaccess only working live
    
    async function postData(url = '', data = {}) {
        // Default options are marked with *
        const response = await fetch(url, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            mode: 'no-cors', // no-cors, *cors, same-origin
            cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
            credentials: 'same-origin', // include, *same-origin, omit
            headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: 'follow', // manual, *follow, error
            referrerPolicy: 'no-referrer', // no-referrer, *no-referrer-when-downgrade, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin, unsafe-url
            body: JSON.stringify(data) // body data type must match "Content-Type" header
        });
        return response.json(); // parses JSON response into native JavaScript objects
    }

    switch (true) {	  		
        case (n >= 0 && _("catcher_id").value == ""): // && str.indexOf("[success] => 1") > 0
        _("successMessage").className = "green_msg";
        _("status_msg").innerHTML = "";
        //$('#contactForm').attr('action', 'https://docs.google.com/forms/d/1lgXi5DDj2hxzwZZe4c5aYn8907kvYbqTeewg5BoJrJE/formResponse'); //
        
        postData(url, { timestamp: timestamp, fname: firstName, lname: lastName, phone: phone, email: email, subject: subject, course_time: course_time, course_date: course_date, message: msg}).then(data => {
            console.log(data); // JSON data parsed by `data.json()` call
            if (done) {
                console.log("Stream complete");
                return;
            }
            else {
                console.log(data_ser);
            }
        });
        
        j.className = "green_msg";
        _("ss-submit").value = _("msg").value;
        submitted = true;
        _("successMessage").innerHTML = "Thank you " + firstName + ", please check your email in 5 minutes for confirmation!";
        _("ss-submit").disabled = true;
        //_("contactForm").reset();		        
        return true;
        break;
        
        case (n < 0):
        _("status_msg").className = "red_msg";
        i.className += "status_msg";
        _("status_msg").innerHTML =  n + ' Please click the "captcha" box';
        _("ss-submit").disabled = true;
        return false;
        break;	  		    
        default:
            return false;
    }
}

</script>
<!-- End Contact Form script -->

<!-- Apple iframe cropping issues script -->
<script>
 $(function(){
if (/iPhone|iPod|iPad/.test(navigator.userAgent))
    $('iframe').wrap(function(){
        var $this = $(this);
        return $('<div />').css({
            width: $this.attr('width'),
            height: $this.attr('height'),
            overflow: 'auto',
            '-webkit-overflow-scrolling': 'touch'
        });
    });
})
</script>
<!-- End Apple iframe cropping issues script -->

<!-- Recaptcha call script -->
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<!-- End Recaptcha call script -->
<style>

#alert_container:disabled {
    opacity: 0;
    cursor: default;
}

#alert_container:enabled {
    opacity: 1;
    cursor: pointer;
}

body {
    font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

.cf_container {
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    /* padding-left: 10px; */
    /* padding-right: 10px; */
}

input#catcher_id {
    visibility: hidden;
}

.cf_frame {
    height: 100%;
    width: 100%;
    box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.33);
    background: linear-gradient( rgba(80,45,87,0.75), rgba(80,45,87,0.95)), no-repeat center center;
    background-size: cover;
    margin-left: auto;
    margin-right: auto;
    border-top: solid 1px rgba(255,255,255,.5);
    border-radius: 5px;
    box-shadow: 1px 3px 7px 1px rgba(0,0,0,0.33);
    overflow: hidden;
    transition: all .5s ease;
}

.cf_form {
    /* width: 100%; */
    height: 100%;
    font-size: 16px;
    top: 0px;
    margin-left: -10px;
    font-weight: 300;
    padding-left: 25px;
    padding-right: 55px;
    padding-top: 10px;
    transition: opacity .5s ease, transform .5s ease;
}

.cf_form-styling {
    font-family: FontAwesome, 'Open Sans', Arial, Helvetica;
    font-style: normal;
    font-weight: normal;
    color: #fff;
    text-decoration: inherit;
    width: 100%;
    height: 35px;
    padding-left: 35px;
    border: none;
    border-radius: 20px;
    margin-bottom: 20px;
    background: rgba(255,255,255,.2);
}

.cf_form-styling option {
    background: rgb(133,108,137);
}

.cf_heading {
    color: #fff;
    font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

center.cf_bottom  {
    margin-left: 20px;
}

.green_msg {
    height: 15px;
    width: auto;
    font-size: 0.8rem;
    color: rgb(103, 250, 28) !important;
    margin-bottom: 115px;
    transition: all ease 0.25s;
}

h2.cf_heading {
    margin-bottom: 0px;
}

h6.cf_heading {
    margin-top: 0px;
    margin-bottom: 10px;
}

i.fa {
    position: absolute;
    margin-left: 15px;
    margin-top: 38px;
    color: rgba(255, 255, 255, 0.5);
}

/*https://www.iconfinder.com/icons/226587/clock_icon*/
input[type="date"]::-webkit-calendar-picker-indicator {
    /*color: rgba(255, 255, 255, 1);*/ 
    opacity: 0.6;
    display: block;
    background: url('../img/icons/calendar_icon2_15x16.png') no-repeat; /*https://mywildalberta.ca/images/GFX-MWA-Parks-Reservations.png*/
    width: 15px;
    height: 15px;
    padding-right: 10px;
    border-width: thin;
}

input::placeholder, textarea::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: rgba(255, 255, 255, 0.5);
    opacity: 1; /* Firefox */
    font-size: 13px;
}

input:-ms-input-placeholder, textarea:-ms-inut-placeholder { /* Internet Explorer 10-11 */
    color: rgba(255, 255, 255, 0.5);
    font-size: 13px;
}

input::-ms-input-placeholder, textarea::-ms-inut-placeholder { /* Microsoft Edge */
    color: rgba(255, 255, 255, 0.5);
    font-size: 13px;
}

select {
    color: #fff !important;
}

select#reply_method {
    padding-right: -32px !important;
    width: 110%;
}

#ss-submit:disabled {
    opacity: 0;
    cursor: default;
}

#ss-submit:enabled {
    opacity: 1;
    cursor: pointer;
}

span#status_msg {
    color: #fff !important;
}

.cf_form label {
    font-weight: 400;
    text-transform: uppercase;
    font-size: 13px;
    padding-left: 15px;
    padding-bottom: 10px;
    color: rgba(255,255,255,.7);
    display: block;
}

.cf_form input:focus, textarea:focus {
    background: rgba(255,255,255,.25);
    border: none !important;
    outline: none !important;
    transition: .2s ease;
}

textarea#msg {
    padding-top: 10px;
    color: #ffffff;
    border-radius: 25px;
    width: 94%;
    height: 50px !important;
    font-size: 13px !important;
}

.btn-submit {
    float: left;
    padding-top: 3px;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 20px;
    margin-top: -8px;
}

.btn-animate {
    float: left;
    font-weight: 700;
    text-transform: uppercase;
    font-size: 13px;
    text-align: center;
    color: rgba(255,255,255, 1);
    padding-top: 0px;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 20px;
    margin-top: 23px;
    background-color: rgba(250,149,28, 1);
    left: 0px;
    top: 0px;
    transition: all .5s ease, top .5s ease .5s, height .5s ease .5s, background-color .5s ease .75s;
}

.btn-submit:hover {
    cursor: pointer;
    background-color: rgb(233, 130, 5);
    transition: background-color .5s;
}

@media screen and (min-width: 772px) {
    #captchabox {
        transform: scale(0.6);
        margin-left: -37%;
    }

    .cf_form label {
        font-size: 9px !important;
    }
}

@media screen and (min-width: 889px) {
    #captchabox {
        transform: scale(0.7);
        margin-left: -20%;
    }
}

@media screen and (max-width: 264px) { /* Will apply to iframe dims not screen dims */
    .cf_form label {
        font-size: 9px !important;
    }
}

</style>

<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(submitted){document.getElementById('successMessage').style.display='block';}" frameborder="0"> 
    <!--onload="if(submitted){document.getElementById('successMessage').style.display='block';}" frameborder="0">-->
</iframe>
