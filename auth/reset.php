<?php 
$page_title = "Reset";
include_once("blocks/header.php");
include_once("blocks/reset_block.php");
?>
<body>
<section id="reset">
<!-- Login form -->
<div class="cf_container">
<div class="cf_frame">
<form action="" id="reset_form" class="cf_form" method="POST" name="form">
    <h2 class="cf_heading">Password Reset</h2>
    <hr class="cf_heading">
    <?php 
    if (isset($result)) echo $result;
    if (!empty($form_err)) echo show_errors($form_err);
    ?>
    <!--<h6 class="cf_heading">Login with your email and password.</h6> -->
    <input type="text" name="catcher" id="catcher_id" class="catcher_class" value=""/>
    
    <div class="input email">
    <i class="fa fa-at"></i>
    <label for="email">Email</label>
    <input class="cf_form-styling" type="email" id="email" name="email" value="" required> <!-- placeholder="@" pattern="[0-9]*" -->
    </div>

    <div class="input phone">
    <i class="fa fa-phone"></i>
    <label for="phone">Phone</label>
    <input class="cf_form-styling" type="tel" id="phone" name="phone" value="" placeholder="Format: 1-234-567-8910" pattern="[0-9]{1,5}-[0-9]{3}-[0-9]{3}-[0-9]{4}" required> <!-- placeholder="@" pattern="[0-9]*" -->
    </div>
    <br>
    <br>
    <center>
    <button class="btn-submit btn-animate" name="reset_btn" value="Reset Passsword" type="submit" id="ss-submit" tabindex=5>Submit</button>
    </div>            
    </center>
    <br>
    <br>
</form>
</div>
</section>
<br>
<!-- End login form -->


</body>
<?php
if ($msg_sent == 1) {
    echo "
    <script type='text/javascript'>
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Reset link sent!',
        text: 'Check your email or phone to verify!',
        showConfirmButton: true,
    }).then(function() {
        window.location.href = 'index.php';
    });
    </script>";
} else if ($msg_sent == 2){
    echo 
    "<script type='text/javascript'>
    Swal.fire({
        position: 'center',
        icon: 'error',
        title: 'Email or phone number not registered!',
        text: 'Please include the phone country code or try again...',
        confirmButtonText: 'Okay',
        });
    </script>";
}
?>
<?php
include_once("blocks/footer.php");
?>



