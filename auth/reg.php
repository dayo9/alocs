<!-- 
Edited: 12/12/2020
By: David Ayodele

References:
Terry Osayawe: https://www.youtube.com/playlist?list=PL9mwDhhLGZilsMTm1POos9aXjvorie3Me
https://thedigitalcraft.com
Niels Harbo - https://nielsharbo.dk
Ashly Lorenzana - http://www.ashlylorenzana.com
-->

<!DOCTYPE html>
<?php
define('scsn', TRUE);
/*include_once('../db_conn/conn_db.php'); // db config outside web dir ($db_conn is varname)*/
//include_once("cap"); 
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link href="../css/font-awesome.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../css/font-awesome-4.7.0/css/font-awesome.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,700" rel="stylesheet" type="text/css">
    <link href= 'https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    <link href= 'https://fonts.googleapis.com/css?family=Oswald:400,700' rel='stylesheet' type='text/css'>
    <!-- jQuery load -->
    <script src="../js/jquery.min.js"></script>
</head>
<body>
<section id="contact_form">

<!--<center> -->

<!-- Contact form - Adapted from Josh Sorosky: https://codepen.io/joshsorosky/pen/gaaBoB -->
<div class="cf_container">
<div class="cf_frame">
<form action="" id="contactForm" class="cf_form" method="POST" target="hidden_iframe" name="form" onsubmit="return validateFormOnSubmit(this); event.preventDefault();">
    <h2 class="cf_heading">User Registration</h2>
    <br>
    <h6 class="cf_heading">Create a new account with the form below and confirm with your email.</h6>
    <input type="text" name="catcher" id="catcher_id" class="catcher_class" value=""/>
    
    <div class="input fname">
    <i class="fa fa-user"></i>
    <label for="entry_953935801">First Name</label>
    <input class="cf_form-styling" type="text" name="entry.953935801" id="name" required> <!-- placeholder="&#xf007;" -->
    </div>

    <div class="input lname">
    <i class="fa fa-user"></i>
    <label for="entry_2130693918">Last Name</label>
    <input class="cf_form-styling" type="text" name="entry.2130693918" id="name3" required> <!-- placeholder="&#xf007;" -->
    </div>

    <div class="input mname">
    <i class="fa fa-user"></i>
    <label for="entry_1109337040">Middle Name or Initial</label>
    <input class="cf_form-styling" type="text" name="entry.1109337040" id="name2"> <!-- placeholder="&#xf007;" -->
    </div>
    <br>
    <br>

    <div class="input address">
    <i class="fa fa-home"></i>
    <label for="entry_577968961">Street Address (ex: 8900 North Central)</label>
    <input class="cf_form-styling" type="text" name="entry.577968961" id="address"> <!-- placeholder="&#xf015;" -->
    </div>

    <div class="input city">
    <i class="fa fa-map-marker"></i>
    <label for="entry_527484810">City</label>
    <input class="cf_form-styling" type="text" name="entry.527484810" id="city"> <!-- placeholder="&#xf041;" -->
    </div>

    <div class="input zip">
    <i class="fa fa-map-marker"></i>
    <label for="entry_1836094135">Zip</label>
    <input class="cf_form-styling" type="text" name="entry.1836094135" id="zip" required> <!-- placeholder="&#xf041;" -->
    </div>

    <div class="input phone">
    <i class="fa fa-phone"></i>
    <label for="entry_1189187901">Phone (ex: (555)555-5555)</label>
    <input class="cf_form-styling" type="tel" name="entry.1189187901" id="phone" required> <!-- placeholder="&#xf095;" -->
    </div>

    <div class="input email">
    <i class="fa fa-at"></i>
    <label for="entry_647512161">Email</label>
    <input class="cf_form-styling" type="email" name="entry.647512161" id="email" required> <!-- placeholder="@" pattern="[0-9]*" -->
    </div>
    <br>
    <br>
    
    <label for="entry_1774485035">Birthday Month only</label>
    <select class="cf_form-styling" name="entry.1774485035">
    <option>- Month -</option>
    <option value="January">January</option>
    <option value="Febuary">February</option>
    <option value="March">March</option>
    <option value="April">April</option>
    <option value="May">May</option>
    <option value="June">June</option>
    <option value="July">July</option>
    <option value="August">August</option>
    <option value="September">September</option>
    <option value="October">October</option>
    <option value="November">November</option>
    <option value="December">December</option>
    </select>

    <label for="entry_700377545">Birthday Day only</label>    
    <select class="cf_form-styling" name="entry.700377545">
    <option>- Day -</option>
    <option value="1">1</option>
    <option value="2">2</option>
    <option value="3">3</option>
    <option value="4">4</option>
    <option value="5">5</option>
    <option value="6">6</option>
    <option value="7">7</option>
    <option value="8">8</option>
    <option value="9">9</option>
    <option value="10">10</option>
    <option value="11">11</option>
    <option value="12">12</option>
    <option value="13">13</option>
    <option value="14">14</option>
    <option value="15">15</option>
    <option value="16">16</option>
    <option value="17">17</option>
    <option value="18">18</option>
    <option value="19">19</option>
    <option value="20">20</option>
    <option value="21">21</option>
    <option value="22">22</option>
    <option value="23">23</option>
    <option value="24">24</option>
    <option value="25">25</option>
    <option value="26">26</option>
    <option value="27">27</option>
    <option value="28">28</option>
    <option value="29">29</option>
    <option value="30">30</option>
    <option value="31">31</option>
    </select>
    <input type="text" name="entry.37808800" id="uname" class="catcher_class" value=""/>
    <input type="text" name="entry.2082575901" id="salt" class="catcher_class" value=""/>

    <div class="input pwdbox">
    <label for="entry_323359491">Password</label>
    <i class="toggle-password fa fa-lock"></i>
    <input class="cf_form-styling toggle" tabindex="5" type="password" name="entry.323359491" id="pwd1" placeholder="click lock to view" required> <!-- placeholder="&#xf023;" pattern="[0-9]*" -->
    </div>
    <div class="input pwdbox">
    <label for="entry_108345931">Confirm Password</label>
    <i class="toggle-password fa fa-lock"></i>
    <input class="cf_form-styling toggle" tabindex="5" type="password" name="entry.108345931" id="pwd2" placeholder="click lock to view" required> <!-- placeholder="&#xf023;" pattern="[0-9]*" -->
    </div>
    <br>
    <br>
    <center>
    <button class="btn-submit btn-animate" type="submit" name="cf_submit" id="ss-submit" tabindex=5>Submit</button>
    <br>
    <br>
    <br>
    <br>
    <span style="color: #fff; font: bold normal 14px/80% Arial, Helvetica, sans-serif; line-height: 14px;">To submit, click the checkbox below<br><br></span>
    <br>
    <div class="g-recaptcha recaptcha" id="captchabox">
    </div>
    <span id="status_msg" name="msg">
    </span>
    <br>
    <br>
    <div id="successMessage" style="display: none;"> 
    <br>
    </div>            
    </center>
</form>
</div>
</section>
<br>
<!-- End contact form -->
</body>

<!-- Google recaptcha PHP call script -->
<script type="text/javascript">

function _(id){ return document.getElementById(id); }

var form = _("contactForm");

function reCaptcha() {
    $.get('../../ignored/ezy_recaptcha_loc'); //ignored/ezy_recaptcha_loc, cap will not work locally, htaccess only working live
}
</script>

<!-- Contact form script -->
<script type="text/javascript">

var submitted = false;
var name = '';
var pass = '';
var str = '';
var n = -1;

var recaptcha_id1;

var today = new Date();
var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

// pass = _("pwd1"); 
// or
// const bcrypt = require('bcrypt')

if(dd<10) {
    dd = '0'+dd
} 
if(mm<10) {
    mm = '0'+mm
} 

today = yyyy + '-' + mm + '-' + dd;

Date.daysBetween = function( date1, date2 ) {
    //Get 1 day in milliseconds
    var one_day=1000*60*60*24;

    // Convert both dates to milliseconds
    var date1_ms = date1.getTime();
    var date2_ms = date2.getTime();

    // Calculate the difference in milliseconds
    var difference_ms = date2_ms - date1_ms;
    
    // Convert back to days and return
    return Math.round(difference_ms/one_day); 
}

$(".toggle-password").click(function() {
    $(".toggle-password").toggleClass("fa-eye fa-lock"); /*$(this).toggleClass("fa-eye-slash") */
    //var input = $($(this).attr("toggle"));
    var input = $(".toggle")

    if (input.attr("type") == "password") {
        input.attr("type", "text");
    } else {
        input.attr("type", "password");
    }
});

async function hash_msg(message) {
  const msgUint8 = new TextEncoder().encode(message);                           // encode as (utf-8) Uint8Array
  const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8);           // hash the message
  const hashArray = Array.from(new Uint8Array(hashBuffer));                     // convert buffer to byte array
  const hashHex = hashArray.map(b => b.toString(16).padStart(2, '0')).join(''); // convert bytes to hex string
  return hashHex;
}

var onloadCallback = function() {
// Renders the HTML element with id 'example1' as a reCAPTCHA widget.
// The id of the reCAPTCHA widget is assigned to 'widgetId1'.
// from https://developers.google.com/recaptcha/docs/display#recaptcha_methods

// If error "uncaught promise, null" received, check all scripts for errors, undefined vars, jQuery loaded!!!

    _("ss-submit").disabled = true;

    recaptcha_id1 = grecaptcha.render(_("captchabox"), {
    'sitekey' : '6LfSSaUZAAAAAOdBsnGmxp9F_MI2yjQyj1Eneu9W', /* live: 6Lf8sVsUAAAAAGSeEocTRqoyho51t1Tp5AInt4kh local: 6LfSSaUZAAAAAOdBsnGmxp9F_MI2yjQyj1Eneu9W*/
    'callback' : imNotARobot,
    'theme' : 'light',
    'type' : 'image'
    });
    
    /*Rescaling recaptcha adapted from: https://stackoverflow.com/questions/22991938/overriding-google-recaptcha-css-to-make-it-responsive */
    function rescaleCaptcha(){
        var width = $('.g-recaptcha').parent().width();
        var scale;
        if (width < 302) {
            scale = width / 302;
        } else {
            scale = 1.0; 
        }

        $('.g-recaptcha').css('transform', 'scale(' + scale + ')');
        $('.g-recaptcha').css('-webkit-transform', 'scale(' + scale + ')');
        $('.g-recaptcha').css('transform-origin', '0 0');
        $('.g-recaptcha').css('-webkit-transform-origin', '0 0');
    }

    rescaleCaptcha();
    $( window ).resize(function() { rescaleCaptcha(); });
}; 

var imNotARobot = function() {
    console.info("Captcha box was clicked");
    console.log(n);
    str = grecaptcha.getResponse(recaptcha_id1);
    console.log(str);

    name = $("#name").val();
    pass1 = $("#pwd1").val();
    pass2 = $("#pwd2").val();
    email = $("#email").val();
    uname = $("#email").val(); // $("#name").val() + $("#phone").val() + $("#name3").val() + $("#zip").val();
    _("uname").value = uname; 

    const myHash = hash_msg(pass1);
   
    var fullName = name.split(' '),
        firstName = fullName[0],
        lastName = fullName[fullName.length - 1];

    $.ajax({
        type: 'POST',
        url: '../../ignored/ezy_recaptcha_loc.php', //ignored/ezy_recaptcha_loc.php, cap will not work locally, htaccess only working live
        data: {pwd1: pass1, pwd2: pass2, email: email, 'g-recaptcha-response': str}, /*appending to POST, old way: "name=" + name + "&g-recaptcha-response=" + str*/
        success: function(data) {		  			
            //console.log(str);
            //console.log(data); // data contains data sent as well any echoed vars from called url.
            data_json = JSON.parse(data);
            /*
            //var data_json = eval(data); // convert string to array
            console.log("myHash: ");
            console.log(myHash);
            console.log("data_json: ");
            console.log(data_json);
            console.log(data_json['success']);
            console.log("pwd1: ");
            console.log(data_json[0]['pwd1']);
            console.log("pwd2: ");
            console.log(data_json[0]['pwd2']);
            console.log(data_json[0]['grecap']);
            console.log(data_json['hostname']);
            */
            n = data_json['success'] ? 1 : 0;
            
            console.log(n);
            if (data_json['success'] && _("pwd1").value != "") {
                //alert('Probably not a robot');
                //console.log("captcha passed");
                _("pwd1").value = data_json[0]['pwd1'];
                _("pwd2").value = data_json[0]['pwd2'];
                _("salt").value = data_json[0]['salt'];
                //console.log(data_json[0]['salt']);

                k = (data_json[0]['pwd1'] == data_json[0]['pwd2']) ? 1 : 0;

                //console.log(k);
                $( "#status_msg" ).removeClass();
                $("#status_msg").addClass( "green_msg" );
                _("ss-submit").disabled = false;
                _("status_msg").innerHTML = "Success! Please click submit!";
            } else {
                // Do something if a robot
                _("status_msg").className = "red_msg";
                _("status_msg").innerHTML = "Error! Please enter a valid password.";
                _("ss-submit").disabled = true;
            }

            /*
            document.getElementById("status_msg").innerHTML = "Captcha check complete!";
            document.getElementById("status_msg").className = ".successMessage";*/
            /*
            if (!course_dates.includes(_("course").value)) {
                _("status_msg").className = "red_msg";
                console.info("Course date slected is " + _("course").value + ". Today is: " + today);
                console.info(Date.daysBetween(date_obj_dob, date_obj_today));
                _("status_msg").innerHTML = "Error: Invalid course date selected: " + _("course").value + ". Please check the course calendar. Today is: " + today + ". Reload page and re-enter info.";
            } 
            
            if (Date.daysBetween(date_obj_dob, date_obj_today) < 5844) {
                _("status_msg").className = "red_msg";
                console.info("Date of birth is " + _("dob").value + ". This less than 16 years from today: " + today);
                _("status_msg").innerHTML = "Error: Date of birth is " + _("dob").value + ". This less than 16 years from today: " + today + ". Reload page and re-enter info.";
            }
            /*
            if (Date.daysBetween(date_obj_dob, date_obj_today) > 5844 && course_dates.includes(_("course").value)) {
                _("ss-submit").disabled = false;   
            }
            */	  	
        }
    })
};        

/*------- 
Contact form validation
--------*/
function validateFormOnSubmit(contact) { // takes contact var as input for legacy browser support funcs
    var i = _("status_msg");
    var j = _("successMessage");
    
    var formdata = new FormData();
    formdata.append( "name", _("name").value );
    formdata.append( "email", _("email").value );
    formdata.append( "phone", _("phone").value );
    
    name = $("#name").val();
    var fullName = name.split(' '),
        firstName = fullName[0],
        lastName = fullName[fullName.length - 1];
    phone = _("phone").value;
    email = _("email").value;

    var form = $("#contactForm"); // must use jquery to access serialize function
    var data_ser = form.serialize(); // Seriale form data

    $.ajax({
        type: 'POST',
        url: 'resources/db.php', // 'store' will not work locally, htaccess only working live
        data: data_ser, /* {'cf1_fname': firstName, 'cf1_lname': lastName, 'cf1_fname': firstName, 'cf1_phone': phone, 'cf1_email': email, 'cf1_reply_method': reply_method, 'cf1_msg': msg}, /*"name=" + name + "&g-recaptcha-response=" + str*/
        success: function(data) {
            console.log(data); // data contains data sent as well any echoed vars from called url.            
            /*
            data_json = JSON.parse(data);
            console.log(data_json);
            console.log(data_json['success']);
            document.getElementById("status_msg").innerHTML = n;
            n = str.indexOf("03A");
            console.log("n = " + n);

            if (data_json['success']) {
                //alert('Probably not a robot');
                console.log("captcha passed");
                $( "#status_msg" ).removeClass();
                $("#status_msg").addClass( "green_msg" );
                _("ss-submit").disabled = false;
                _("status_msg").innerHTML = "Success! Please double check your email and submit!";
            } else {
                // Do something if a robot
                _("ss-submit").disabled = true;
            }
            */
        }
    })

    switch (true) {	  		
        case (n >= 0 && k > 0): // && str.indexOf("[success] => 1") > 0
        console.log(n);
        _("successMessage").className = "green_msg";
        _("status_msg").innerHTML = "";
        $('#contactForm').attr('action', 'https://docs.google.com/forms/d/e/1FAIpQLSftZ4qKAZmk5o31aBVTra6CBEE1Bv3GNj99njDSPoKAvuajdw/formResponse'); //
        j.className = "green_msg";
        _("ss-submit").value = _("uname").value;
        submitted = true;
        _("successMessage").innerHTML = "Thank you " + firstName + ", we've sent an email confirmation to " + email + ". If you experience errors, please email staff@starcanyon.org";
        _("ss-submit").disabled = true;
        //_("contactForm").reset();		        
        return true;
        break;
        
        case (n < 0):
        _("status_msg").className += "red_msg";
        i.className += "status_msg";
        _("status_msg").innerHTML =  'Error: Please click the "captcha" check box';
        _("ss-submit").disabled = true;
        return false;
        break;

        case (k < 1):
        _("status_msg").className += "red_msg";
        i.className += "status_msg";
        _("status_msg").innerHTML =  '<br>Error: Please check if passwords match.';
        _("ss-submit").disabled = true;
        return false;
        break;

        default:
            return false;
    }
}

</script>
<!-- End Contact Form script -->

<!-- Apple iframe cropping issues script -->
<script>
 $(function(){
if (/iPhone|iPod|iPad/.test(navigator.userAgent))
    $('iframe').wrap(function(){
        var $this = $(this);
        return $('<div />').css({
            width: $this.attr('width'),
            height: $this.attr('height'),
            overflow: 'auto',
            '-webkit-overflow-scrolling': 'touch'
        });
    });
})
</script>
<!-- End Apple iframe cropping issues script -->

<!-- Recaptcha call script -->
<script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit" async defer></script>
<!-- End Recaptcha call script -->
<style>

body {
    font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

.cf_container {
    width: 100%;
    padding-top: 10px;
    padding-bottom: 10px;
    /* padding-left: 10px; */
    /* padding-right: 10px; */
}

input#catcher_id, input#uname, input#salt {
    visibility: hidden;
}

.cf_frame {
    height: 100%;
    width: 100%;
    box-shadow: 1px 1px 5px 1px rgba(0, 0, 0, 0.33);
    background: linear-gradient( rgba(80,45,87,0.75), rgba(80,45,87,0.95)), no-repeat center center;
    background-size: cover;
    margin-left: auto;
    margin-right: auto;
    border-top: solid 1px rgba(255,255,255,.5);
    border-radius: 5px;
    box-shadow: 1px 3px 7px 1px rgba(0,0,0,0.33);
    overflow: hidden;
    transition: all .5s ease;
}

.cf_form {
    /* width: 100%; */
    height: 100%;
    font-size: 16px;
    top: 0px;
    font-weight: 300;
    padding-left: 25px;
    padding-right: 45px;
    padding-top: 10px;
    transition: opacity .5s ease, transform .5s ease;
}

.cf_form-styling {
    font-family: FontAwesome, 'Open Sans', Arial, Helvetica;
    font-style: normal;
    font-weight: normal;
    color: #fff;
    text-decoration: inherit;
    width: 100%;
    height: 35px;
    padding-left: 35px;
    border: none;
    border-radius: 20px;
    margin-bottom: 20px;
    background: rgba(255,255,255,.2);
}

.cf_form-styling option {
    background: rgb(133,108,137);
}

.cf_heading {
    color: #fff;
    font-family: 'Open Sans', Arial, Helvetica, sans-serif;
}

.cf_form input:focus, textarea:focus {
    background: rgba(255,255,255,.25);
    border: none !important;
    outline: none !important;
    transition: .2s ease;
}

.red_msg {
    height: 15px;
    width: auto;
    font-size: 0.8rem;
    color: rgb(250, 0, 0) !important;
    margin-bottom: 50px;
    transition: all ease 0.25s;
}

span#status_msg {
    color: rgb(255, 150, 50);
    margin-top: 48px !important;
}

.green_msg {
    height: 15px;
    width: auto;
    font-size: 0.8rem;
    color: rgb(103, 250, 28) !important;
    margin-bottom: 50px;
    transition: all ease 0.25s;
}

h2.cf_heading {
    margin-bottom: 0px;
    /* color: rgba(250,149,28, 1); */
    text-shadow: 0.5px 0.5px 0.5px rgb(0 0 0 / 33%);
}

h6.cf_heading {
    margin-top: 0px;
    margin-bottom: 10px;
}

i.fa {
    position: absolute;
    margin-left: 15px;
    margin-top: 38px;
    color: rgba(255, 255, 255, 0.5);
}

i.fa.fa-lock, i.fa.fa-eye {
    position: absolute;
    margin-top: 10px;
    margin-left: 15px;
    color: rgba(255, 255, 255, 0.5);
    cursor: pointer;
}

/*https://www.iconfinder.com/icons/226587/clock_icon*/
input[type="date"]::-webkit-calendar-picker-indicator {
    /*color: rgba(255, 255, 255, 1);*/ 
    opacity: 0.6;
    display: block;
    background: url('../img/icons/calendar_icon2_15x16.png') no-repeat; /*https://mywildalberta.ca/images/GFX-MWA-Parks-Reservations.png*/
    width: 15px;
    height: 15px;
    padding-right: 10px;
    border-width: thin;
}

input::placeholder, textarea::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
    color: rgba(255, 255, 255, 0.5);
    opacity: 1; /* Firefox */
}

input:-ms-input-placeholder, textarea:-ms-inut-placeholder { /* Internet Explorer 10-11 */
    color: rgba(255, 255, 255, 0.5);
}

input::-ms-input-placeholder, textarea::-ms-inut-placeholder { /* Microsoft Edge */
    color: rgba(255, 255, 255, 0.5);
}

select {
    color: #fff !important;
}

#ss-submit:disabled {
    opacity: 0;
    cursor: default;
}

#ss-submit:enabled {
    opacity: 1;
    cursor: pointer;
}

label {
    font-weight: 400;
    text-transform: uppercase;
    font-size: 13px;
    padding-left: 15px;
    padding-bottom: 10px;
    color: rgba(255,255,255,.7);
    display: block;
}

textarea#msg {
    padding-top: 10px;
    color: #ffffff;
    border-radius: 25px;
    width: 94%;
    height: 50px !important;
}

.btn-submit {
    float: left;
    padding-top: 0px;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 20px;
    margin-top: -8px;
}

.btn-animate {
    float: left;
    font-weight: 700;
    text-transform: uppercase;
    font-size: 13px;
    text-align: center;
    color: rgba(255,255,255, 1);
    padding-top: 0px;
    width: 100%;
    height: 35px;
    border: none;
    border-radius: 20px;
    margin-top: 23px;
    background-color: rgba(250,149,28, 1);
    left: 0px;
    top: 0px;
    transition: all .5s ease, top .5s ease .5s, height .5s ease .5s, background-color .5s ease .75s;
}

.btn-submit:hover {
    cursor: pointer;
    background-color: rgb(233, 130, 5);
    transition: background-color .5s;
}

/*
@media screen and (min-width: 772px) {
  #captchabox {
    transform: scale(0.6);
    margin-left: -37%;
  }
}

@media screen and (min-width: 889px) {
  #captchabox {
    transform: scale(0.7);
    margin-left: -20%;
  }
}
*/
</style>

<iframe name="hidden_iframe" id="hidden_iframe" style="display:none;" onload="if(submitted){document.getElementById('successMessage').style.display='block';}" frameborder="0"> 
    <!--onload="if(submitted){document.getElementById('successMessage').style.display='block';}" frameborder="0">-->
</iframe>
</html>

<!--Notes

https://stackoverflow.com/questions/9068767/php-mysql-create-database-if-not-exists
https://stackoverflow.com/questions/8705195/how-do-i-select-a-mysql-database-to-use-with-pdo-in-php
https://stackoverflow.com/questions/11928136/return-one-value-from-database-with-mysql-php-pdo/11928442 

-->