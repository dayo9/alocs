<?php 
$page_title = "Home";
include_once("blocks/header.php");
include_once("resources/utils.php");


$_SESSION['user_ip'] = get_user_ip();
?>

<body>
    <main>
    <div class="container">
    <section class="col col-lg-7">
    
    <h2><?php if (isset($_SESSION['fname'])) echo $_SESSION['fname']."'s "; ?>Homepage</h2>
    <hr>
    <?php if (!isset($_SESSION['email'])): ?>
    <p>You are NOT signed in. <a href="login.php">Login</a> 
    <br>
    Not yet a member? <a href="signup.php">Sign-Up</a>
    </p>
    
    <?php else: $username = $_SESSION['email']; ?>
    <p>You are logged in as <?php echo $username ?>. <a href="account.php">View account</a></p>
    <?php endif ?>
    <br>
    <br>

    </section>
    </div>
    </main>

</body>

<?php
include_once("blocks/footer.php");
?>


