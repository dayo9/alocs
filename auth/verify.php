<?php 
include_once('resources/sessions.php');
include_once('resources/utils.php');

if (isset($_GET['id'])) {
    $sec_user_id = $_GET['id'];
    $id = explode("i_am_the_greenest_geek", base64_decode($sec_user_id))[1];
    //$id = decryptString($enc_id, "user_verification"); 
    
    $query = "UPDATE users SET verified = :verified WHERE id = :id";
    $statement = $db->prepare($query);
    $statement->execute(array(':verified' => "1", ":id" => $id));

    if ($statement->rowCount() == 1) {
        $result = log_msg("Verified! You can now <a href='login.php'>login</a>.", "green"); 
    } else {
        $result = log_msg("Not Verified: Please contact us, your account could not be verified.", "red");
    }
}
?>

<?php 
$page_title = "Account Verification";
include_once("blocks/header.php");
?>
<body>
<div class="container">
<section class="col col-lg-7">
<h2>Account verification status</h2>
<hr>
<span>
<?php if(isset($result)) echo $result; ?>
</span>

<?php
include_once("blocks/footer.php");
?>






