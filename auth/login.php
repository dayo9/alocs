<?php 
define('alocs', TRUE);
$page_title = "Login";
include_once("blocks/login_block.php");  // since login_block calls setcookie() &
// setcookie() defines a cookie to be sent along with the HTTP response headers, 
// NO HTML can come before login_block (CANNOT MODIFY HEADER... error occurs).
// since we're using include_once, utils can be duplicated without error
include_once("blocks/header.php");
?>
<body>
<section id="login">
<!-- Login form -->
<div class="cf_container">
<div class="cf_frame">
<form action="" id="login_form" class="cf_form" method="POST" name="form">
    <h2 class="cf_heading">User Login</h2>
    <hr class="cf_heading">
    <?php 
    if (isset($result)) echo $result;
    if (!empty($form_err)) echo show_errors($form_err);
    ?>
    <!--<h6 class="cf_heading">Login with your email and password.</h6> -->
    <input type="text" name="catcher" id="catcher_id" class="catcher_class" value=""/>
    
    <div class="input email">
    <i class="fa fa-at"></i>
    <label for="email">Email</label>
    <input class="cf_form-styling" type="email" id="email" name="email" value="" required> <!-- placeholder="@" pattern="[0-9]*" -->
    </div>

    <div class="input pwdbox">
    <label for="password">Password</label>
    <i class="toggle-password fa fa-lock"></i>
    <input class="cf_form-styling toggle" tabindex="5" type="password" name="password" value="" id="pwd1" placeholder="click lock to view" required> <!-- placeholder="&#xf023;" pattern="[0-9]*" -->
    </div>

    <div class="checkbox">
    <label>
    <input type="checkbox" name="rem_check" value="yes"> Remember me
    </label>
    </div>
    <div>
    <label>
    <a href="reset.php">Forgot Password?</a>
    </label>
    </div>

    <center>
    <button class="btn-submit btn-animate" name="login_btn" value="" type="submit" id="ss-submit" tabindex=5>Submit</button>
    </div>            
    </center>
    <br>
    <br>
</form>
</div>
</section>
<br>
<!-- End login form -->
</body>

<?php 
if ( (isset($_SESSION['email']) || $valid_cookie) ) {
    echo 
    "
    <script>
    Swal.fire({
        position: 'center',
        icon: 'success',
        title: 'Welcome $fname!',
        text: 'You\'re being logged in...',
        showConfirmButton: false,
        timer: 3000
    });

    setTimeout(function(){
        window.location.href = 'index.php';
    }, 3000);
    </script>

    ";
} 
?>

<?php
include_once("blocks/footer.php");
?>



